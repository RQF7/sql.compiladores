#
#  Copyright (C) 2017 Ricardo Quezada Figueroa,
#  Laura Natalia Borbolla Palacios,
#  Daniel Ayala Zamorano,
#  Marco Rubio Cortés.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#  Programas y compiladores
compilador_c := gcc
compilador_y := bison
compilador_l := flex
compilador_t := pdflatex

#  Opciones de compilación
banderas_ligado      := -lncursesw
banderas_c_propias   := -Wall
banderar_c_generales := -ggdb

#  Carpetas
carpeta_obj := obj
carpeta_fnt := fnt
carpeta_bin := bin

#  Productos
ejecutable := $(carpeta_bin)/sql

#  Código fuente
fuentes := sql.c \
		   programa.c \
		   simbolo.c \
		   vector_str.c \
		   lista.c \
		   table_struct.c \
		   operaciones_comunes.c \
		   operaciones_definicion.c \
		   operaciones_manipulacion.c \
	   	   analizador_sintactico.tab.c \
	   	   analizador_lexico.tab.c

objetos := $(addprefix $(carpeta_obj)/, $(fuentes:.c=.o))

#  Carpeta fuente a VPATH
VPATH := $(carpeta_fnt)

#  Objetivo principal
$(ejecutable): $(carpeta_obj) $(carpeta_bin) $(objetos)
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) \
		$(objetos) $(banderas_ligado) -o $@

#  Objetivos secundarios
$(carpeta_obj)/sql.o: sql.c analizador_sintactico.tab.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/programa.o: programa.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/simbolo.o: simbolo.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/operaciones_comunes.o: operaciones_comunes.c
		$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/operaciones_definicion.o: operaciones_definicion.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/operaciones_manipulacion.o: operaciones_manipulacion.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/lista.o: lista.c
		$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/table_struct.o: table_struct.c
		$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/vector_str.o: vector_str.c
	$(compilador_c) $(banderas_c_propias) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/analizador_sintactico.tab.o: analizador_sintactico.tab.c
	$(compilador_c) $(banderar_c_generales) -c $< -o $@

$(carpeta_obj)/analizador_lexico.tab.o: analizador_lexico.tab.c
	$(compilador_c) $(banderar_c_generales) -c $< -o $@

#  Objetivos de bison y flex
$(carpeta_fnt)/analizador_sintactico.tab.c: analizador_sintactico.y
	$(compilador_y) -d -o $@ $<
	mv $(carpeta_fnt)/analizador_sintactico.tab.h $(carpeta_fnt)/include/

$(carpeta_fnt)/analizador_lexico.tab.c: analizador_lexico.l
	$(compilador_l) $<
	mv lex.yy.c $@

#  Manuales
manual_tecnico.pdf: manuales/manual_tecnico.tex
	$(compilador_t) -output-directory $(carpeta_obj) -shell-escape $<
	mv $(carpeta_obj)/$@ ./$@

manual_usuario.pdf: manuales/manual_usuario.tex
	$(compilador_t) -output-directory $(carpeta_obj) -shell-escape $<
	mv $(carpeta_obj)/$@ ./$@

#  Creación de carpetas
$(carpeta_obj):
	mkdir -p $@

$(carpeta_bin):
	mkdir -p $@

#  Objetivos phony
run: $(ejecutable)
	./$<

clean:
	rm -Rfv $(carpeta_obj) $(carpeta_bin) \
		$(carpeta_fnt)/analizador_sintactico.tab.* \
		$(carpeta_fnt)/analizador_lexico.tab.c
