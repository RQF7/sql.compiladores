#ifndef __OPERACIONES_MANIPULACION_H__
#define __OPERACIONES_MANIPULACION_H__

#include "operaciones_comunes.h"
#include "lista.h"
#include "table_struct.h"

/* Inserta una fila de valores, si es exitiso retorna 1 */
int insertValues(const char *DBName, const char *TableName,  List Fields);

/* Combierte dos cadenas en una row, (un contiene losnombres de los campos y otra la info */
Row stringsToRow(char *Name, char *Info, char *d);

/* Carga una tabla con la informacion de un archivo con el formato preestablecido */
Table LoadFileToTable(const char *Filename);

/* Carga una tabla de una base de datos a una estructura table */
Table LoadDB(const char *DBName, const char *TableName);

/* Regresa una lista con los campos que no estan el la lista dada */
List fieldsNoEquals(List Header, Row Data);

/* Carga la los campos seleccionados en una tabla, en caso de que sea vacia la lista regresa todos */
Table selectValues(const char *DBName, const char *TableName, List Fields);

/* Elimina todos los registros que tengan, en el campo dado, el valor especificado */
int deleteValues(const char *DBName, const char *TableName, const char *FieldName, const char *Value);

/* Revisa si la fila, en el campo especificado, tiene el valor dado */
int isAMatch(int num_field, const char* value, char *row);

#endif
