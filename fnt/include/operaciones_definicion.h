#ifndef __OPERACIONES_DEFINICION_H__
#define __OPERACIONES_DEFINICION_H__

#include "operaciones_comunes.h"
#include "lista.h"

/* Crea la carpeta raíz donde se guardarán las bases de datos */
void create_DBFolder();

/* Genera una nueva base de datos, retorna 0 si la BD ya existe */
int newDataBase(const char* DBName);

/* Elimina una nueva base de datos, regresa 0 si la BD no existió */
int deleteDataBase(const char* DBName);

/* Renombra una base de datos, regresa 0 si la BD no existió */
int renameDataBase(const char *DBName, const char *new_DBName);

/* Regresa en una lista las bases de datos. Lee los archivos del directorio*/
List showDatabases();

/* Crea una nueva tabla, si ya existe una con el mismo nombre retorna 0 */
int newTable(const char *DBName, const char *TableName, List Columns);

/* Elimina una tabla, si no ya existe una, retorna 0 */
int deleteTable(const char *DBName, const char *TableName);

/* Renombra una tabla, regresa 0 si hubo errores*/
/* RQF: ¿Qué errores? */
/* Si la base o la tabla no existen, o si no pudo renombrar el descriptor o la tabla*/
int renameTable(const char *DBName, const char *TableName, const char *new_TableName);

/* Regresa en una lista las tablas de una base de datos. Lee los archivos del directorio*/
List showTables(const char *DBName);

/* Crea un campo en una tabla dada, regresa 0 si hubo un error */
int newField(const char *DBName, const char *TableName, const char *FieldName);

/* Cambiar descriptor para agregar un campo */
int addFieldInDescriptor(const char *DBName, const char *TableName, const char *FieldName, int num_cols);

/* Cambiar tabla para agregar un campo */
int addFieldInTable(const char *DBName, const char *TableName, const char *FieldName, int num_cols);

/* Renombra un campo de una tabla, regresa 0 si hubo errores*/
int renameField(const char *DBName, const char *TableName, const char *FieldName, const char *new_FieldName);

/* Renombra un campo de una tabla, regresa 0 si hubo errores*/
int renameFieldinDescriptor(const char *DBName, const char *TableName, const char *new_FieldName, int found);

/* Renombra un campo de una tabla, regresa 0 si hubo errores*/
int renameFieldinTable(const char *DBName, const char *TableName, const char *new_FieldName, int found, int num_cols);

/* Elimina el campo de una tabla, regresa 0 si hubo errores*/
int deleteField(const char *DBName, const char *TableName, const char *FieldName);

/* Elimina el campo de una tabla en el Descriptor, regresa 0 si hubo errores*/
int deleteFieldInDescriptor(const char *DBName, const char *TableName, int found);

/* Elimina el campo de una tabla en el archivo de tabla, regresa 0 si hubo errores*/
int deleteFIeldInTable(const char *DBName, const char *TableName, int found, int num_cols);

/* Regresa una lista con los campos de una tabla ordenados*/
List showFields(const char *DBName, const char *TableName);

#endif
