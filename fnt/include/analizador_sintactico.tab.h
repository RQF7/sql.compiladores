/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_FNT_ANALIZADOR_SINTACTICO_TAB_H_INCLUDED
# define YY_YY_FNT_ANALIZADOR_SINTACTICO_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    CREATE = 258,
    DROP = 259,
    ALTER = 260,
    RENAME = 261,
    TO = 262,
    USE = 263,
    DATABASE = 264,
    TABLE = 265,
    COLUMN = 266,
    PRIMARY = 267,
    KEY = 268,
    SHOW = 269,
    DATABASES = 270,
    TABLES = 271,
    FOREIGN = 272,
    REFERENCES = 273,
    COLUMNS = 274,
    ADD = 275,
    MODIFY = 276,
    CHANGE = 277,
    INT = 278,
    DOUBLE = 279,
    CHAR = 280,
    VARCHAR = 281,
    SELECT = 282,
    FROM = 283,
    WHERE = 284,
    INSERT = 285,
    INTO = 286,
    UPDATE = 287,
    SET = 288,
    DELETE = 289,
    VALUES = 290,
    DEFAULT = 291,
    IN = 292,
    IS = 293,
    BOL_TRUE = 294,
    BOL_FALSE = 295,
    BETWEEN = 296,
    LIKE = 297,
    ID = 298,
    NUMERO = 299,
    NUMERO_FLOTANTE = 300,
    CADENA = 301,
    OR = 302,
    AND = 303,
    XOR = 304,
    NOT = 305,
    MAIG = 306,
    MEIG = 307,
    DIST = 308,
    CI = 309,
    CD = 310
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 40 "fnt/analizador_sintactico.y" /* yacc.c:1909  */

	Simbolo *simbolo;
	Instruccion *instruccion;
	int num_elementos;

#line 116 "fnt/analizador_sintactico.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_FNT_ANALIZADOR_SINTACTICO_TAB_H_INCLUDED  */
