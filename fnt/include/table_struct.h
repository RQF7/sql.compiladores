#ifndef __TABLE_STRUCT_H__
#define __TABLE_STRUCT_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct SRow{
    char *Name;
    char *Info;
    struct SRow *Next;
}*Row;

typedef struct STable{
    int RowNum;
    Row Row;
    struct STable *Next;
}*Table;

/*..........................................................................................*/
Row emptyRow();
Row addField(Row r, char *name, char *info);
int isRowEmpty(Row r);
char *headOfRowInfo(Row r);
char *headOfRowName(Row r);
Row leftOfRow(Row r);
void printRow(Row r);
void printRow2(Row r);
int rowLength(Row r);
int existField(Row r, char *name);
char *searchField(Row r, char *name);
Row removeField(Row r, char *name);
/*..........................................................................................*/
Table emptyTable();
Table addRow(Table t, int rnum, Row row);
int isTableEmpty(Table t);
int headOfTableNum(Table t);
Row headOfTableRow(Table t);
Table leftOfTable(Table t);
void printTable(Table t);
int tableRows(Table t);
int existRow(Table t, int rnum);
Row searchRow(Table t, int rnum);
Table removeRow(Table t, int rnum);
/*..........................................................................................*/

#endif
