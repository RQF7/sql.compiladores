#ifndef __OPERACIONES_COMUNES_H__
#define __OPERACIONES_COMUNES_H__

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <curses.h>
#include <dirent.h>
#include "lista.h"

#define SPACE_LINE "=================================================="
#define DB_FOLDER "Bases_de_datos"
#define EMPTY_FIELD "##NADA##"

/* Verifica si un archivo o carpeta ya existe, Si(1) No(0)*/
int existObject(const char *ObjectName);

/* Regresa la direccion del descriptor de una base de datos */
const char *getDescriptorName(const char *DBName);

/* Regresa la direccion de una tabla de una base de datos */
const char *getDirTable(const char *DBName, const char *TableName);

/* Regresa la direccion de una base de datos */
const char *getDirDB(const char *DBName);

/* Retorna un atributo de una tabla */
int getDBAttibute(const char *DBName, const char *TableName, int Attr);

/* Retorna el numero de columnas de una tabla */
int getNumCols(const char *DBName, const char *TableName);

/* Retorna el numero de filas de una tabla */
int getNumRows(const char *DBName, const char *TableName);

/* Regresa un arreglo bidimensional con los campos de una tabla */
char** getFields(const char *DBName, const char *TableName);

/* Incrementa el numero de filas de una tabla en el descriptor de la BD */
int incNumRows(const char *DBName, const char *TableName);

/* Suma a el numero de filas de una tabla en el descriptor de la BD */
int changeNumRows(const char *DBName, const char *TableName, int change);

/* Incrementa el numero de columnas de una tabla en el descriptor de la BD */
int changeNumCols(const char *DBName, const char *TableName, int change);

/* Elimina los tabuladores y saltos de línea */
char* removeTabs(char *string);

/* Elimina saltos de linea */
char* removeJumps(char *string);

/* Verifica si una cadena termina con un sufijo dado */
int string_ends_with(const char * str, const char * suffix);

/* Verifica si una cadena comienza con un prefijo dado */
int string_starts_with(const char* string, const char* prefix);

#endif
