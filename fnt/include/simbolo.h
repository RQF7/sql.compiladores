/*
 *  simbolo.h
 *  Archivo de cabecera del módulo de tabla de símbolos de intérprete SQL.
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano,
 *  Marco Rubio Cortés.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SIMBOLO_H__
#define __SIMBOLO_H__

/* Tabla de símbolos: lista simplemente ligada. */
typedef struct Simbolo {
    char *nombre;
    int tipo;
    union {
        int valor;
        double (*funcion)();
    } u;
    struct Simbolo *siguiente;
} Simbolo;

Simbolo *instalar (char *nombre, int tipo, int valor);
Simbolo *buscar (char *nombre);

#endif
