/*
 *  sql.h
 *  Archivo de cabecera de funciones y variables comúnes al entorno de
 *  ejecución.
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SQL_H__
#define __SQL_H__

#include "vector_str.h"
#include "lista.h"
#include "table_struct.h"
#include <curses.h>

void warning (char *mensaje, char *detalle);
void error_en_ejecucion (char *mensaje, char *detalle);
void error_sql ();
void prompt ();
void mensaje_exito (char *mensaje);
void mensaje_fallo (char *mensaje);
void imprimir_lista (List lista, char *encabezado);
void imprimir_tabla (Table tabla);

void bases_seleccionar ();
void bases_crear (char *base);
void bases_borrar (char *base);
void bases_limpiar_seleccion ();
void bases_renombrar (char *origen, char *destino);

/* Variables globales */
int numero_de_linea;
int *linea;
Vector_str *historial;
int dentro_de_definicion;
char *base_seleccionada;
WINDOW *panel, *interprete;

#endif
