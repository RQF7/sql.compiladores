/*
 *  programa.h
 *  Archivo de cabecera del módulo de programa de intérprete SQL.
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROGRAMA_H__
#define __PROGRAMA_H__

#include "simbolo.h"
#include <stdarg.h>
#define DEBUG 0			/* Controla la impresión de mensajes de estado. */

typedef union Dato_de_pila {
    int valor;
    Simbolo *simbolo;
} Dato_de_pila;

/* Tipo de dato Instrucción: apuntador a función que no toma argumentos y
 * regresa void. */
typedef void (*Instruccion)();
#define DETENER (Instruccion) 0

/* Variables globales definidas en programa.c. */
extern Instruccion programa[];
extern Instruccion *apuntador_de_programa;

/* Operaciones generales sobre RAM y pila. */
void iniciar_codificacion();
Dato_de_pila sacar_de_pila();
void meter_a_pila(Dato_de_pila d);
Instruccion *codificar(int n, ...);
void ejecutar(Instruccion *instruccion);

/* Instrucciones de programa. */
void crear_base ();
void borrar_base ();
void renombrar_base ();
void seleccionar_base ();
void crear_tabla ();
void borrar_tabla ();
void renombrar_tabla ();
void codigo_alter ();
void agregar_campo ();
void borrar_campo ();
void renombrar_campo ();
void mostrar_bases ();
void mostrar_tablas ();
void mostrar_columnas ();
void meter_cadena ();
void meter_entero ();
void codigo_select_simple ();
void codigo_insert_simple ();
void codigo_insert_compuesto ();
void codigo_delete ();

#endif
