#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "../operaciones_comunes.h"
#include "../operaciones_definicion.h"
#include "../operaciones_manipulacion.h"
#include "../lista.h"
#include "../table_struct.h"

int main(int argc, char const *argv[]){

    if (argc < 3){
        printf("Uso %s BaseDatos tabla1 \n", argv[0]);
        exit(0);
    }
    int exito;
    exito = newDataBase(argv[1]);
    if(exito) printf("Nueva base de datos creada\n");

    List largs = emptyList();
    largs = addValue(largs,1,"#Numero");
    largs = addValue(largs,2,"Direccion");
    largs = addValue(largs,3,"Correo");
    largs = addValue(largs,4,"Nombre");
    largs = addValue(largs,5,"Taller");
    largs = addValue(largs,6,"Peso");
    largs = addValue(largs,7,"Quesos");
    largs = addValue(largs,8,"Signo");
    largs = addValue(largs,9,"Edad");

    exito = newTable(argv[1],argv[2],largs);
    if(exito) printf("Nueva tabla creada\n");

    List lf1 = emptyList();
    lf1 = addValue(lf1,1,"11");
    lf1 = addValue(lf1,2,"Por alla A");
    lf1 = addValue(lf1,3,"Daz79");
    lf1 = addValue(lf1,4,"N1");
    lf1 = addValue(lf1,5,"Electronica");
    lf1 = addValue(lf1,6,"92");
    lf1 = addValue(lf1,7,"Oaxaca");
    lf1 = addValue(lf1,8,"+");
    lf1 = addValue(lf1,9,"10");

    List lf2 = emptyList();
    lf2 = addValue(lf2,1,"1");
    lf2 = addValue(lf2,2,"Por alla B");
    lf2 = addValue(lf2,3,"Daz79");
    lf2 = addValue(lf2,4,"N2");
    lf2 = addValue(lf2,5,"Electronica");
    lf2 = addValue(lf2,6,"95");
    lf2 = addValue(lf2,7,"Oaxaca");
    lf2 = addValue(lf2,8,"+");
    lf2 = addValue(lf2,9,"20");

    List lf3 = emptyList();
    lf3 = addValue(lf3,1,"1");
    lf3 = addValue(lf3,2,"Por alla C");
    lf3 = addValue(lf3,3,"Daz79");
    lf3 = addValue(lf3,4,"N3");
    lf3 = addValue(lf3,5,"Electronica");
    lf3 = addValue(lf3,6,"78");
    lf3 = addValue(lf3,7,"Oaxaca");
    lf3 = addValue(lf3,8,"+");
    lf3 = addValue(lf3,9,"30");

    insertValues(argv[1], argv[2], lf1);
    insertValues(argv[1], argv[2], lf2);
    insertValues(argv[1], argv[2], lf3);

    List LIST = emptyList();

    printf("T1\n");
    LIST = emptyList();
    LIST = addValue(LIST,1,"#Numero");
    LIST = addValue(LIST,2,"Direccion");
    LIST = addValue(LIST,3,"Correo");
    printTable(selectValues(argv[1],argv[2],LIST));

    printf("T2\n");
    LIST = emptyList();
    LIST = addValue(LIST,1,"#Numero");
    LIST = addValue(LIST,3,"Correo");
    LIST = addValue(LIST,4,"Nombre");
    LIST = addValue(LIST,6,"Peso");
    LIST = addValue(LIST,8,"Signo");
    printTable(selectValues(argv[1],argv[2],LIST));

    printf("T3\n");
    LIST = emptyList();
    LIST = addValue(LIST,1,"#Numero");
    LIST = addValue(LIST,6,"Peso");
    LIST = addValue(LIST,7,"Quesos");
    LIST = addValue(LIST,8,"Signo");
    LIST = addValue(LIST,9,"Edad");
    printTable(selectValues(argv[1],argv[2],LIST));

    printf("T4\n");
    LIST = emptyList();
    LIST = addValue(LIST,2,"Direccion");
    LIST = addValue(LIST,1,"#Numero");
    LIST = addValue(LIST,7,"Quesos");
    LIST = addValue(LIST,8,"Signo");
    LIST = addValue(LIST,4,"Nombre");
    LIST = addValue(LIST,9,"Edad");
    LIST = addValue(LIST,6,"Peso");
    printTable(selectValues(argv[1],argv[2],LIST));

    printf("T5\n");
    LIST = emptyList();
    LIST = addValue(LIST,1,"#Numero");
    LIST = addValue(LIST,3,"Correo");
    LIST = addValue(LIST,5,"Taller");
    LIST = addValue(LIST,7,"Quesos");
    LIST = addValue(LIST,9,"Edad");
    printTable(selectValues(argv[1],argv[2],LIST));

    printf("T6\n");
    LIST = emptyList();
    printTable(selectValues(argv[1],argv[2],LIST));
    printf("\n");

    return 1;
}
