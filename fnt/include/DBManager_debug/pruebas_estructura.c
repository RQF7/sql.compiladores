#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "../operaciones_comunes.h"
#include "../operaciones_definicion.h"
#include "../operaciones_manipulacion.h"
#include "../lista.h"

/* argv[1] = DB  argv[2] = Tabla */
/* NOTA PRIMARY KEY = #CAMPO */
int main(int argc, char const *argv[]){

    List ltabla = emptyList();
    ltabla = addValue(ltabla,1, (char*) "#Numero");
    ltabla = addValue(ltabla,2, (char*) "Direccion");
    ltabla = addValue(ltabla,3, (char*) "Correo");
    ltabla = addValue(ltabla,4, (char*) "Nombre");
    ltabla = addValue(ltabla,5, (char*) "Taller");
    ltabla = addValue(ltabla,6, (char*) "Peso");

    List lvalores = emptyList();
    lvalores = addValue(lvalores,1, (char*) "1");
    lvalores = addValue(lvalores,2, (char*) "Por allá");
    lvalores = addValue(lvalores,3, (char*) "correo@gmail.com");
    lvalores = addValue(lvalores,4, (char*) "Daniel");
    lvalores = addValue(lvalores,5, (char*) "Electrónica");
    lvalores = addValue(lvalores,6, (char*) "88");

    List lvalores2 = emptyList();
    lvalores2 = addValue(lvalores2, 1, (char*) "1");
    lvalores2 = addValue(lvalores2, 2, (char*) "Por allá");
    lvalores2 = addValue(lvalores2, 3, (char*) "correo@gmail.com");
    lvalores2 = addValue(lvalores2, 4, (char*) "Laura");
    lvalores2 = addValue(lvalores2, 5, (char*) "Electrónica");
    lvalores2 = addValue(lvalores2, 6, (char*) "88");

    List lvacia = emptyList();
    lvacia = addValue(lvacia, 1, (char*) "1");
    lvacia = addValue(lvacia, 2, (char*) "2");

    List lvacia2 = emptyList();
    lvacia2 = addValue(lvacia2, 1, (char*) "2");
    lvacia2 = addValue(lvacia2, 2, (char*) "2");

    printf("DB Active\n");
    int exito;
    char ch1[15];

    exito = newDataBase("BaseOriginal");
    if(exito) printf("1. Nueva base de datos creada\n");
    exito = newTable("BaseOriginal", "Tabla1", ltabla);
    if(exito) printf("2. Nueva tabla creada\n");
    exito = insertValues("BaseOriginal", "Tabla1",lvalores);
    exito = insertValues("BaseOriginal", "Tabla1",lvalores2);
    if(exito) printf("2,1. Datos insertados en Tabla1.\n");
    exito = deleteValues("BaseOriginal", "Tabla1", "Nombre", "Laura");
    if(exito) printf("Datos borrados de tabla Tabla1\n" );
    scanf("%s", ch1);
    exito = deleteTable("BaseOriginal", "Tabla1");
    if(exito) printf("Tabla Tabla1 borrada\n");
    exito =  newTable("BaseOriginal", "Tabla2", ltabla);
    if(exito) printf("3. Tabla Tabla2 creada\n");
    exito =  newTable("BaseOriginal", "Tabla3", ltabla);
    if(exito) printf("4. Tabla Tabla3 creada\n");
    exito = insertValues("BaseOriginal", "Tabla3",lvalores);
    exito = insertValues("BaseOriginal", "Tabla3",lvalores);
    if(exito) printf("2,1. Datos insertados en Tabla3.\n");
    exito = renameDataBase("BaseOriginal", "BaseRenombrada");
    if(exito) printf("5. Base de datos renombrada.\n");
    exito = renameTable("BaseRenombrada", "Tabla3", "Tabla4");
    if(exito) printf("6. Tabla Tabla3 renombrada a Tabla4.\n");
    scanf("%s",ch1);
    exito = renameField("BaseRenombrada", "Tabla4", "Taller", "CampoRenombrado");
    if(exito)   printf("7. Campo renombrado en Tabla4.\n");
    scanf("%s",ch1);
    exito = newTable("BaseRenombrada", "vacia", emptyList());
    if(exito) printf("8. Nueva tabla vacia creada\n");
    exito = newField("BaseRenombrada", "vacia", "Campo nuevo Nuevo");
    exito = newField("BaseRenombrada", "vacia", "Campo nuevo Nuevo 1");

    exito = insertValues("BaseRenombrada", "vacia", lvacia);
    exito = insertValues("BaseRenombrada", "vacia", lvacia);
    exito = insertValues("BaseRenombrada", "vacia", lvacia);
    exito = insertValues("BaseRenombrada", "vacia", lvacia2);
    exito = insertValues("BaseRenombrada", "vacia", lvacia2);
    if(exito)   printf("8,1. Campos y datos agregados a vacia.\n");
    scanf("%s", ch1);

    exito = newField("BaseRenombrada", "Tabla4", "Campo nuevo Nuevo 2");
    exito = newField("BaseRenombrada", "vacia", "Campo nuevo Nuevo 2");
    if(exito)   printf("9. Campos agregados en vacia y Tabla4.\n");
    /* Espera hasta que se introduce algo para poder ver los cambios al eliminar. */


    scanf("%s",ch1);

    exito = deleteField("BaseRenombrada", "vacia", "Campo nuevo Nuevo");
    if(exito) printf("10. Campo eliminado en tabla vacia\n");
    List lexito = showFields("BaseRenombrada", "Tabla4");
    if(lexito!=NULL) printf("10,1. Columnas en tabla Tabla4\n");
    printList(lexito);
    lexito = showFields("BaseRenombrada", "vacia");
    if(lexito!=NULL) printf("10,2. Columnas en tabla vacia\n");
    printList(lexito);
    lexito = showTables("BaseRenombrada");
    if(lexito!=NULL) printf("10,3. Tablas en BaseRenombrada\n");
    printList(lexito);

    exito = newField("BaseRenombrada", "vacia", "Campo nuevo Nuevo 4");
    exito = deleteValues("BaseRenombrada", "vacia", "Campo nuevo Nuevo 4", EMPTY_FIELD);
    if(exito)   printf("Campos eliminados en tabla vacia\n" );
/*    exito = deleteTable("BaseRenombrada", "vacia");
    if(exito) printf("11. Tabla vacia eliminada.\n");
*/    exito = newDataBase("BaseOriginal2");
    if(exito) printf("11, 1. Nueva base de datos creada\n");
    exito = newDataBase("BaseOriginal3");
    if(exito) printf("11, 2. Nueva base de datos creada\n");
    exito = newDataBase("BaseOriginal4");
    if(exito) printf("11, 3. Nueva base de datos creada\n");
    lexito = showDatabases();
    if(lexito!=NULL) printf("11, 4. Bases de datos.\n");
    //printList(lexito);
    free(lexito);
    scanf("%s",ch1);

    exito = deleteDataBase("BaseRenombrada");
    exito = deleteDataBase("BaseOriginal2");
    exito = deleteDataBase("BaseOriginal3");
    exito = deleteDataBase("BaseOriginal4");

    if(exito) printf("12. Bases borradas exitosamente\n");

    return 1;
}
