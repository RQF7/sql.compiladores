#ifndef __LISTA_H__
#define __LISTA_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct nodol{  	/*.............................. estructura de nodo */
    int id;
    char *dataX;
    struct nodol *next;
}*List;

/*..........................................................................................*/
List emptyList();
List addValue(List l, int id, char *e);
int isEmpty(List l);
char *headOfListData(List l);
int headOfListId(List l);
List leftOfList(List l);
void printList(List l);
int listSize(List l);
int existItem(List l, int id);
char *search(List l, int id);
List removeValue(List l, int id);
char *ListToString(List l, char *d, char *dir);
/*..........................................................................................*/

#endif
