#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/table_struct.h"


/*========================================================= FINCIONES DE ROW */
/*===========================================================================*/
Row emptyRow()
{return NULL;}

/*===========================================================================*/
Row addField(Row r, char *name, char *info)
{
    char *Name = malloc(sizeof(char) * strlen(name));
	strcpy(Name,name);
    char *Info = malloc(sizeof(char) * strlen(info));
	strcpy(Info,info);
	Row t = (Row) malloc(sizeof(struct SRow));
	t->Name=Name;
	t->Info=Info;
	t->Next=r;
	return t;
}

/*===========================================================================*/
int isRowEmpty(Row r)
{return r==NULL;}

/*===========================================================================*/
char *headOfRowInfo(Row r)
{return r->Info;}

/*===========================================================================*/
char *headOfRowName(Row r)
{return r->Name;}

/*===========================================================================*/
Row leftOfRow(Row r)
{return r->Next;}

/*===========================================================================*/
void printRow(Row r)
{
	if(!isRowEmpty(r)){
		printRow(leftOfRow(r));
        printf("[%s : %s]\n",headOfRowName(r),headOfRowInfo(r));
    }
}

/*===========================================================================*/
void printRow2(Row r)
{
	if(!isRowEmpty(r)){
		printRow2(leftOfRow(r));
        printf("[%s : %s] ",headOfRowName(r),headOfRowInfo(r));
    }
}

/*===========================================================================*/
int rowLength(Row r)
{
	if(isRowEmpty(r)) return 0;
	else return 1+rowLength(leftOfRow(r));
}

/*===========================================================================*/
int existField(Row r, char *name)
{
    if(isRowEmpty(r))     				        return 0;
    else if(!strcmp(headOfRowName(r),name))     return 1;
    else                				        return existField(leftOfRow(r),name);
}

/*===========================================================================*/
char *searchField(Row r, char *name)
{
    if (isRowEmpty(r))     				        return "NOT_FOUND";
    else if(!strcmp(headOfRowName(r),name))		return headOfRowInfo(r);
    else                				        return searchField(leftOfRow(r),name);
}

/*===========================================================================*/
Row removeField(Row r, char *name)
{
    if (isRowEmpty(r))
		return r;
    else if(!strcmp(headOfRowName(r),name))
		return removeField(leftOfRow(r),name);
    else
		return addField(removeField(leftOfRow(r),name),headOfRowName(r),headOfRowInfo(r));
}


/*===========================================================================*/
/*======================================================= FINCIONES DE TABLE */

/*===========================================================================*/
Table emptyTable()
{return NULL;}

/*===========================================================================*/
Table addRow(Table t, int rnum, Row row)
{
    Table tx = (Table) malloc(sizeof(struct STable));
	tx->RowNum=rnum;
	tx->Row=row;
	tx->Next=t;
	return tx;
}

/*===========================================================================*/
int isTableEmpty(Table t)
{return t==NULL;}

/*===========================================================================*/
int headOfTableNum(Table t)
{return t->RowNum;}

/*===========================================================================*/
Row headOfTableRow(Table t)
{return t->Row;}

/*===========================================================================*/
Table leftOfTable(Table t)
{return t->Next;}

/*===========================================================================*/
void printTable(Table t)
{
	if(!isTableEmpty(t)){
		printTable(leftOfTable(t));
        printRow2(headOfTableRow(t));
        printf("\n");
    }
}

/*===========================================================================*/
int tableRows(Table t)
{
	if(isTableEmpty(t)) return 0;
	else return 1+tableRows(leftOfTable(t));
}

/*===========================================================================*/
int existRow(Table t, int rnum)
{
    if(isTableEmpty(t))     		    return 0;
    else if(rnum == headOfTableNum(t))  return 1;
    else                				return existRow(leftOfTable(t),rnum);
}

/*===========================================================================*/
Row searchRow(Table t, int rnum)
{
    if (isTableEmpty(t))     			return emptyRow();
    else if(rnum == headOfTableNum(t))	return headOfTableRow(t);
    else                				return searchRow(leftOfTable(t),rnum);
}

/*===========================================================================*/
Table removeRow(Table t, int rnum)
{
    if (isTableEmpty(t))
		return t;
    else if(rnum == headOfTableNum(t))
		return removeRow(leftOfTable(t),rnum);
    else
		return addRow(removeRow(leftOfTable(t),rnum),headOfTableNum(t),headOfTableRow(t));
}

/*===========================================================================*/
