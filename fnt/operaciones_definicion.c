#include "include/operaciones_definicion.h"
#include "include/lista.h"

void create_DBFolder()
{
    char *mkdir = (char*) malloc(sizeof(const char) * (strlen(DB_FOLDER) + 8 ));
    strcpy(mkdir, "mkdir ");
    strcat(mkdir, DB_FOLDER);
    system(mkdir);
    return;
}

/* Genera una nueva base de datos, retorna 0 si la BD ya existe */
//=============================================================================
int newDataBase(const char* DBName)
{
    /* Crear, si no existe, el directorio raíz */
    if(!existObject(DB_FOLDER)) create_DBFolder();
    if(existObject(getDirDB(DBName))) return 0;

    /* Creacion de un directorio para la base de datos */
    char *command_mkdir;
    command_mkdir = (char*) malloc(sizeof(const char) * strlen(getDirDB(DBName)) + 10);
    strcpy(command_mkdir,"mkdir ");
    strcat(command_mkdir,getDirDB(DBName));
    system(command_mkdir);

    /* Se pone un encabezado en un archivo de descripcion */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"w");
	if(Descriptor == NULL){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}

    fprintf(Descriptor, "%s%s\n",SPACE_LINE,SPACE_LINE);
    fprintf(Descriptor, "DBNAME :: %s\n",DBName);
    fprintf(Descriptor, "%s%s\n",SPACE_LINE,SPACE_LINE);
    fprintf(Descriptor, "%s%s\n",SPACE_LINE,SPACE_LINE);
    fclose(Descriptor);

    return 1;
}

/* Elimina una nueva base de datos, regresa 0 si la BD no existió */
//=============================================================================
int deleteDataBase(const char* DBName)
{
    if(!existObject(getDirDB(DBName))) return 0;

    /* Eliminar archivos de la base de datos */
    char *command_rm, *command_rmdir;

    /* Eliminar los archivos dentro de la carpeta */
    command_rm = (char*) malloc(sizeof(const char) * (strlen(getDirDB(DBName))+20));
    strcpy(command_rm,"rm -rf ");
    strcat(command_rm,getDirDB(DBName));
    strcat(command_rm,"/*.txt");
    system(command_rm);

    /* Trasladar la carpeta vacía a .basura */
    if(!existObject(".basura")) system("mkdir .basura");
    command_rmdir = (char*) malloc(sizeof(const char) * strlen(getDirDB(DBName)) + 20);
    strcpy(command_rmdir, "mv -f ");
    strcat(command_rmdir, getDirDB(DBName));
    strcat(command_rmdir, " .basura/");
    system(command_rmdir);

    free(command_rmdir);    free(command_rm);

    return 1;
}

/* Renombra una base de datos, regresa 0 si hubo errores*/
//=============================================================================
int renameDataBase(const char *DBName, const char *new_DBName)
{
    if(!existObject(getDirDB(DBName))) return 0;
    int exito;
    /* Actualizar Descriptor */

    /* Se abre el descriptor y un archivo temporal */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error (1): %s\n", strerror(errno));
		return 0;
	}
    char line[500];
    char *new_name = (char*)malloc(sizeof(char) * (12 + strlen(new_DBName)));
    strcpy(new_name, "DBNAME :: ");
    strcat(new_name, new_DBName);
    strcat(new_name, "\n");

    /* Copiar y actualizar archivo */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s",line);
    fprintf(Descriptorv2, "%s", new_name);
    fgets(line, sizeof(line), Descriptor);
    while (fgets(line, sizeof(line), Descriptor)) {
        fprintf(Descriptorv2, "%s",line);
    }
    fclose(Descriptor); fclose(Descriptorv2);

    /* Eliminar descriptor viejo*/
    exito = remove(getDescriptorName(DBName));
    if(exito == -1){
        //printf("Error (2): %s\n", strerror(errno));
		return 0;
    }

    /* Renombrar directorio */
    exito = rename(getDirDB(DBName), getDirDB(new_DBName));
    if(exito == -1){
        //printf("Error (3): %s\n", strerror(errno));
		return 0;
    }

    /* Renombra descriptor nuevo */
    exito = rename("temporal.txt",getDescriptorName(new_DBName));
    if(exito == -1){
        //printf("Error (4): %s\n", strerror(errno));
		return 0;
    }

    return 1;
}

/* Regresa en una lista las bases de datos. Lee los archivos del directorio*/
//=============================================================================
List showDatabases()
{
    DIR *dp;
    struct dirent *ep;
    char *objname;
    dp = opendir (DB_FOLDER);
    List databases = emptyList();
    int i = 0;
    if (dp != NULL){
        while ((ep = readdir (dp))){
            objname = ep->d_name;
            if( (!string_ends_with(objname, ".txt")) && (!string_ends_with(objname, ".")) ){
                databases = addValue(databases, i, objname);
                i++;
            }
        }
        (void) closedir (dp);
    }

    return databases;
}

/* Crea una nueva tabla, si ya existe una con el mismo nombre retorna 0 */
//=============================================================================
int newTable(const char *DBName, const char *TableName, List Columns)
{
    if(existObject(getDirTable(DBName,TableName)))  return 0;

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Table = fopen(getDirTable(DBName,TableName),"w");
    FILE *Descriptor = fopen(getDescriptorName(DBName),"a");
    if((Descriptor == NULL) || (Table == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}

    /* Lectura de los campos dados como argumentos */
    int NColumns = listSize(Columns);

    /* Encabezado descriptor */
    fprintf(Descriptor, "TABLE :: %s\n%d Columns\n0 Rows\n",TableName,NColumns);
    fprintf(Descriptor, "=====>\n");

    /* Encabezado tabla*/
    fprintf(Table, "%s%s\n",SPACE_LINE,SPACE_LINE);

    while(!isEmpty(Columns)){
        //printf("Lista>>%s\n", headOfListData(Columns));
        fprintf(Descriptor, "\t\t%s\n",headOfListData(Columns));
        fprintf(Table, "%s\t\t",headOfListData(Columns));
        Columns = leftOfList(Columns);
    }
    /* Fin de Descriptor */
    fprintf(Descriptor, "%s%s\n",SPACE_LINE,SPACE_LINE);
    /* Fin de tabla */
    fprintf(Table, "\n%s%s\n",SPACE_LINE,SPACE_LINE);

    /* Se cierran los archivos */
    fclose(Descriptor);
    fclose(Table);
    return 1;
}

/* Elimina una tabla, si no ya existe una, retorna 0 */
//=============================================================================
int deleteTable(const char *DBName, const char *TableName)
{
    if(!existObject(getDirTable(DBName,TableName)))  return 0;

    /* Eliminar archivo de tabla */
    char *command_rm = (char*) malloc(sizeof(const char) * (strlen(DBName) + strlen(TableName) ) + 10);
    strcpy(command_rm,"rm ");
    strcat(command_rm,getDirTable(DBName,TableName));
    system(command_rm);

    /* Se abre el descriptor y un archivo temporal */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("bffr.bffr","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}

    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    char *separador = (char*) malloc(sizeof(char) * strlen(SPACE_LINE) * 2 + 10);
    strcpy(separador, SPACE_LINE);
    strcat(separador, SPACE_LINE);
    strcat(separador, "\n");

    /*Copia todo al archivo principal excepto el segmento delimitado por
      table_lable y separador.*/
    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(table_label,line)){
            while (fgets(line, sizeof(line), Descriptor)) {
                if(!strcmp(separador,line)){
                    fgets(line, sizeof(line), Descriptor);
                    break;
                }
            }
        }fprintf(Descriptorv2, "%s",line);
    }

    //free(table_label);
    /* Cerrar archivos y renombrar temporal como el archivo descriptor*/
    fclose(Descriptor); fclose(Descriptorv2);
    remove(getDescriptorName(DBName));
    rename("bffr.bffr",getDescriptorName(DBName));
    return 1;
}

/* Renombra una tabla, regresa 0 si hubo errores*/
//=============================================================================
int renameTable(const char *DBName, const char *TableName, const char *new_TableName)
{
    if(!existObject(getDirDB(DBName))) return 0;
    if(!existObject(getDirTable(DBName, TableName))) return 0;

    int exito;
    /* Actualizar Descriptor */
    /* Se abre el descriptor y un archivo temporal */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error (1): %s\n", strerror(errno));
		return 0;
	}
    char line[500];
    char *new_name = (char*)malloc(sizeof(char) * (12 + strlen(new_TableName)));
    strcpy(new_name, "TABLE :: ");
    strcat(new_name, new_TableName);
    strcat(new_name, "\n");

    char *old_name = (char*)malloc(sizeof(char) * (12 + strlen(TableName)));
    strcpy(old_name, "TABLE :: ");
    strcat(old_name, TableName);
    strcat(old_name, "\n");

    /* Copiar y actualizar archivo */
    while (fgets(line, sizeof(line), Descriptor)) {
        if(strcmp(line, old_name) == 0){
            fprintf(Descriptorv2, "%s", new_name);
        }
        else
            fprintf(Descriptorv2, "%s",line);
    }
    fclose(Descriptor); fclose(Descriptorv2);

    /* Renombrar descriptor */
    exito = rename("temporal.txt", getDescriptorName(DBName));
    if(exito == -1){
        //printf("Error (2): %s\n", strerror(errno));
		return 0;
    }

    /* Renombra archivo de tabla */
    exito = rename(getDirTable(DBName, TableName), getDirTable(DBName, new_TableName));
    if(exito == -1){
        //printf("Error (3): %s\n", strerror(errno));
		return 0;
    }

    return 1;
}

/* Regresa en una lista las tablas de una base de datos. Lee los archivos del directorio*/
//=============================================================================
List showTables(const char *DBName)
{
    DIR *dp;
    struct dirent *ep;
    char *objname;
    dp = opendir (getDirDB(DBName));
    List tables = emptyList();
    int i = 0, longitud;
    if (dp != NULL){
        while ((ep = readdir (dp))){
            objname = ep->d_name;
            if(string_ends_with(objname, ".txt") && !string_starts_with(objname, "DB_Descriptor_")){
                longitud = strlen(objname);
                /* Quitar el .txt */
                objname[longitud-4] = '\0';
                tables = addValue(tables, i, objname);
                i++;
            }
        }
        (void) closedir (dp);
    }

    return tables;
}

/* Crea un campo en una tabla dada, regresa 0 si hubo un error */
//=============================================================================
int newField(const char *DBName, const char *TableName, const char *FieldName)
{
    if(!existObject(getDirDB(DBName))) return 0;
    if(!existObject(getDirTable(DBName,TableName)))  return 0;

    int num_cols = getNumCols(DBName, TableName);
    char **fields = getFields(DBName, TableName);
    int found=0;
    for(int i=0; i<num_cols; i++){
        if(!strcmp(FieldName, fields[i])){
            found = i+1;
            break;
        }
    }
    if(found)return 0;
    if(!addFieldInDescriptor(DBName, TableName, FieldName, num_cols))   return 0;
    if(!addFieldInTable(DBName, TableName, FieldName, num_cols+1))   return 0;
    return 1;
}

/* Cambiar descriptor para agregar un campo */
//=============================================================================
int addFieldInDescriptor(const char *DBName, const char *TableName, const char *FieldName, int num_cols)
{
    if(!changeNumCols(DBName, TableName, 1))    return 0;
    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}

    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(line, table_label)){
            for(int i=0; i<num_cols+4; i++){
                fprintf(Descriptorv2, "%s", line);
                fgets(line, sizeof(line), Descriptor);
            }
            fprintf(Descriptorv2, "\t\t%s\n", FieldName);
            fprintf(Descriptorv2, "%s", line);
            fgets(line, sizeof(line), Descriptor);
        }
        fprintf(Descriptorv2, "%s", line);
    }
    fclose(Descriptor); fclose(Descriptorv2);
    free(table_label);
    rename("temporal.txt", getDescriptorName(DBName));
    return 1;
}

/* Cambiar tabla para agregar un campo */
//=============================================================================
int addFieldInTable(const char *DBName, const char *TableName, const char *FieldName, int num_cols)
{
    char line[500];
    char line2[500];
    char **fields = getFields(DBName, TableName);
    strcpy(line2, "");
    for(int i=0; i<num_cols; i++){
        strcat(line2, fields[i]);
        strcat(line2, "\t\t");
    }

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptor = fopen(getDirTable(DBName, TableName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}
    /* Cambiar encabezado */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);

    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s\n", line2);

    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);

    while (fgets(line, sizeof(line), Descriptor)) {
        strcpy(line2, "");
        char* temp = removeJumps(line);
        strcat(line2, temp);
        strcat(line2, EMPTY_FIELD);
        strcat(line2, "\t\t");
        fprintf(Descriptorv2, "%s\n", line2);
        free(temp);
    }

    fclose(Descriptor); fclose(Descriptorv2);
    rename("temporal.txt", getDirTable(DBName, TableName));
    return 1;
}

/* Renombra el campo de una tabla, regresa 0 si hubo errores*/
//=============================================================================
int renameField(const char *DBName, const char *TableName, const char *FieldName, const char *new_FieldName)
{
    if(!existObject(getDirDB(DBName))) return 0;
    if(!existObject(getDirTable(DBName, TableName))) return 0;
    int num_cols = getNumCols(DBName, TableName);
    char **fields = getFields(DBName, TableName);
    int found=0;
    for(int i=0; i<num_cols; i++){
        if(!strcmp(FieldName, fields[i])){
            found = i+1;
            break;
        }
    }
    if(!found)  return 0;
    if(!renameFieldinDescriptor(DBName, TableName, new_FieldName, found))   return 0;
    if (!renameFieldinTable(DBName, TableName, new_FieldName, found, num_cols))   return 0;
    return 1;
}

/* Renombra un campo de una tabla, regresa 0 si hubo errores*/
//=============================================================================
int renameFieldinDescriptor(const char *DBName, const char *TableName, const char *new_FieldName, int found)
{
    /* Si sí existe, cambiarlo, si no, regresar error.*/
    FILE *Descriptor = fopen(getDescriptorName(DBName), "r");
    FILE *Descriptorv2 = fopen("temporal.txt", "w");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(line, table_label)){
            for(int i=0; i<found+3; i++){
                fprintf(Descriptorv2, "%s", line);
                fgets(line, sizeof(line), Descriptor);
            }
            fprintf(Descriptorv2, "\t\t%s\n", new_FieldName);
            fgets(line, sizeof(line), Descriptor);
        }
        fprintf(Descriptorv2, "%s", line);
    }
    fclose(Descriptor); fclose(Descriptorv2);
    free(table_label);
    rename("temporal.txt", getDescriptorName(DBName));
    return 1;

}

/* Renombra un campo de una tabla, regresa 0 si hubo errores*/
//=============================================================================
int renameFieldinTable(const char *DBName, const char *TableName, const char *new_FieldName, int found, int num_cols)
{
    char line[500];
    char line2[500];
    char **fields = getFields(DBName, TableName);
    strcpy(line2, "");
    for(int i=0; i<num_cols; i++){
        strcat(line2, fields[i]);
        strcat(line2, "\t\t");
    }

    /* Se abre la tabla */
    FILE *Descriptor = fopen(getDirTable(DBName, TableName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}
    /* Cambiar encabezado */

    /* Separador*/
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);

    /* Encabezado */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s\n", line2);

    /* Separador */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);

    while (fgets(line, sizeof(line), Descriptor)) {
        fprintf(Descriptorv2, "%s", line);
    }

    fclose(Descriptor); fclose(Descriptorv2);
    rename("temporal.txt", getDirTable(DBName, TableName));
    return 1;
}

/* Elimina el campo de una tabla, regresa 0 si hubo errores*/
//=============================================================================
int deleteField(const char *DBName, const char *TableName, const char *FieldName)
{
    if(!existObject(getDirDB(DBName))) return 0;
    if(!existObject(getDirTable(DBName, TableName))) return 0;
    int num_cols = getNumCols(DBName, TableName);
    char **fields = getFields(DBName, TableName);
    int found=0;
    for(int i=0; i<num_cols; i++){
        if(!strcmp(FieldName, fields[i])){
            found = i+1;
            break;
        }
    }

    if(!found)  return 0;
    if(!deleteFIeldInTable(DBName, TableName, found-1, num_cols)) return 0;
    if(!deleteFieldInDescriptor(DBName, TableName, found))  return 0;

    return 1;
}

/* Elimina el campo de una tabla en el Descriptor, regresa 0 si hubo errores*/
//=============================================================================
int deleteFieldInDescriptor(const char *DBName, const char *TableName, int found)
{
    /* Si sí existe, cambiarlo, si no, regresar error.*/
    if(!changeNumCols(DBName, TableName, -1))    return 0;
    FILE *Descriptor = fopen(getDescriptorName(DBName), "r");
    FILE *Descriptorv2 = fopen("temporal.txt", "w");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(line, table_label)){
            for(int i=0; i<found+3; i++){
                fprintf(Descriptorv2, "%s", line);
                fgets(line, sizeof(line), Descriptor);
            }
            fgets(line, sizeof(line), Descriptor);
        }
        fprintf(Descriptorv2, "%s", line);
    }
    fclose(Descriptor); fclose(Descriptorv2);
    free(table_label);
    rename("temporal.txt", getDescriptorName(DBName));
    return 1;
}

/* Elimina el campo de una tabla en el archivo de tabla, regresa 0 si hubo errores*/
//=============================================================================
int deleteFIeldInTable(const char *DBName, const char *TableName, int found, int num_cols)
{
    char line[500];
    char line2[500];
    char *tok;
    char **fields = getFields(DBName, TableName);
    int limSuperior = found, limInferior = found+1;
    strcpy(line2, "");

    for(int i=0; i<limSuperior; i++){
        strcat(line2, fields[i]);
        strcat(line2, "\t\t");
    }
    for(int i=limInferior; i<num_cols; i++){
        strcat(line2, fields[i]);
        strcat(line2, "\t\t");
    }

    /* Se abre la tabla */
    FILE *Descriptor = fopen(getDirTable(DBName, TableName),"r");
    FILE *Descriptorv2 = fopen("temporal.txt","w");
    if((Descriptor == NULL) || (Descriptorv2 == NULL)){
		//printf("Error: %s\n", strerror(errno));
		return 0;
	}
    /* Cambiar encabezado */

    /* Separador*/
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);
    /* Encabezado */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s\n", line2);

    /* Separador */
    fgets(line, sizeof(line), Descriptor);
    fprintf(Descriptorv2, "%s", line);

    /* Regresar si no hay entradas en la tabla */
    if(getNumRows(DBName, TableName)>0){
        while (fgets(line, sizeof(line), Descriptor)) {
            strcpy(line2, "");
            /* Copiar campos antes de la columna eliminada */
            tok = strtok(line, "\t\t");
            for(int i=0; i<limSuperior; i++){
                strcat(line2, tok);
                strcat(line2, "\t\t");
                tok = strtok(NULL, "\t\t");
            }
            /* Copiar campos después de la columna eliminada */
            tok=strtok(NULL, "\t\t");
            for(int i=limInferior; i<num_cols; i++){
                strcat(line2, tok);
                strcat(line2, "\t\t");
                tok = strtok(NULL, "\t\t");
                if(tok==NULL)   break;
            }
            fprintf(Descriptorv2, "%s\n", removeJumps(line2));
        }
    }

    fclose(Descriptor); fclose(Descriptorv2);
    rename("temporal.txt", getDirTable(DBName, TableName));
    return 1;
}

/* Regresa una lista con los campos de una tabla ordenados*/
//=============================================================================
List showFields(const char *DBName, const char *TableName)
{
    int num_cols = getNumCols(DBName, TableName);
    char **fields = getFields(DBName, TableName);
    List campos = emptyList();
    for(int i=0; i<num_cols; i++){
        campos = addValue(campos, i, fields[i]);
    }
    free(fields);
    return campos;
}

//=============================================================================
