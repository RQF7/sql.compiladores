#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/lista.h"

/*======================================================== FINCIONES DE LIST */

/*===========================================================================*/
List emptyList(){return NULL;}

/*===========================================================================*/
List addValue(List l, int id, char *e)
{
	char *x = (char*)malloc(sizeof(char) * strlen(e));
	strcpy(x,e);
	List t = (List) malloc(sizeof(struct nodol));
	t->id=id;
	t->dataX=x;
	t->next=l;
	return t;
}

/*===========================================================================*/
int isEmpty(List l)
{return l==NULL;}

/*===========================================================================*/
char *headOfListData(List l)
{return l->dataX;}

/*===========================================================================*/
int headOfListId(List l)
{return l->id;}

/*===========================================================================*/
List leftOfList(List l)
{return l->next;}

/*===========================================================================*/
void printList(List l)
{
	if(!isEmpty(l)){
		printList(leftOfList(l));
        printf("Row: %d \t %s\n",headOfListId(l),headOfListData(l));
    }
}

/*===========================================================================*/
int listSize(List l){
	if (isEmpty(l)) return 0;
	else return 1+listSize(leftOfList(l));
}

/*===========================================================================*/
int existItem(List l, int id)
{
    if (isEmpty(l))     				return 0;
    else if (id==headOfListId(l))     	return 1;
    else                				return existItem(leftOfList(l),id);
}

/*===========================================================================*/
char *search(List l, int id)
{
    if (isEmpty(l))     				return "NULL";
    else if (id==headOfListId(l))		return headOfListData(l);
    else                				return search(leftOfList(l),id);
}

/*===========================================================================*/
List removeValue(List l, int id)
{
    if (isEmpty(l))
		return l;
    else if (id==headOfListId(l))
		return removeValue(leftOfList(l),id);
    else
		return addValue(removeValue(leftOfList(l),id),headOfListId(l),headOfListData(l));
}

/*===========================================================================*/
char *ListToString(List l, char *d, char *dir)
{
	if(isEmpty(l))		return "";
	if(listSize(l)==1){
		char *a;
		int size = strlen(d) + strlen(headOfListData(l));
		a = (char*) malloc(sizeof(char*) * size);
		strcat(a,headOfListData(l));
		strcat(a,d);
		return a;
	}else{
		char *a;
		int size = strlen(d) + strlen(headOfListData(l)) + strlen(ListToString(leftOfList(l),d,dir));
		a = (char*) malloc(sizeof(char*) * size);
		if(!strcmp(dir,"left")){
			strcat(a,headOfListData(l));
			strcat(a,d);
			strcat(a,ListToString(leftOfList(l),d,dir));
		}else if(!strcmp(dir,"right")){
			strcat(a,ListToString(leftOfList(l),d,dir));
			strcat(a,headOfListData(l));
			strcat(a,d);
		}return a;
    }
}

/*===========================================================================*/
