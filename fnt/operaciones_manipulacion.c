#include "include/operaciones_manipulacion.h"
#include "include/lista.h"
#include "include/table_struct.h"

/* Inserta una fila de valores, si es exitiso retorna 1 */
//=============================================================================
int insertValues(const char *DBName, const char *TableName,  List Fields)
{
    if(!existObject(getDescriptorName(DBName)))             return  0;
    if(!existObject(getDirTable(DBName,TableName)))         return  0;
    if(listSize(Fields) != getNumCols(DBName,TableName))    return -1;

    /* Se abre la tabla */
    FILE *Table = fopen(getDirTable(DBName,TableName),"a");

    /* Se agregan los datos y se actualiza el descriptor */
//    char *fieldstabs = ListToString(Fields,"\t\t","right");
    while(!isEmpty(Fields)){
        fprintf(Table,"%s\t\t", headOfListData(Fields));
        Fields = leftOfList(Fields);
    }
    incNumRows(DBName,TableName);
    fprintf(Table,"\n");
//    if(listSize(Fields)) free(fieldstabs);

    /* Se cierran los archivos */
    fclose(Table);
    return 1;
}

/* Combierte dos cadenas en una row, (un contiene losnombres de los campos y otra la info */
//=============================================================================
Row stringsToRow(char *Name, char *Info, char *d){
    char *name = (char*) malloc(sizeof(char*) * strlen(Name));
    char *info = (char*) malloc(sizeof(char*) * strlen(Info));
    char *tokenname, *tokeninfo;
    strcpy(name,Name);
    strcpy(info,Info);
    char *savename, *saveinfo;
    Row ROW = emptyRow();
    tokenname = strtok_r(name,d,&savename);
    tokeninfo = strtok_r(info,d,&saveinfo);
    while(tokenname != NULL && tokeninfo != NULL){
		if(!strcmp(tokeninfo, "##NADA##"))
			tokeninfo = strdup("");
        ROW = addField(ROW,tokenname,tokeninfo);
        tokenname = strtok_r(NULL, d, &savename);
        tokeninfo = strtok_r(NULL, d, &saveinfo);
    }
    free(name);
    free(info);
    return ROW;
}

/* Carga una tabla con la informacion de un archivo con el formato preestablecido */
//=============================================================================
Table LoadFileToTable(const char *Filename){
    FILE *FileX = fopen(Filename,"r");
    Table TABLE = emptyTable();

    char Header[1000],line[1000];
    fgets(line, sizeof(line), FileX);
    fgets(line, sizeof(line), FileX);
    strcpy(Header,line);
    strtok(Header,"\n");
    fgets(line, sizeof(line), FileX);

    int i = 0;
    while(fgets(line, sizeof(line), FileX)){
        strtok(line,"\n");
        TABLE = addRow(TABLE,i++,stringsToRow(Header,line,"\t\t"));
    }
    fclose(FileX);
    return TABLE;
}

/* Carga una tabla de una base de datos a una estructura table */
//=============================================================================
Table LoadDB(const char *DBName, const char *TableName){
    if(!existObject(getDescriptorName(DBName)))             return  emptyTable();
    if(!existObject(getDirTable(DBName,TableName)))         return  emptyTable();
    return LoadFileToTable(getDirTable(DBName,TableName));
}

/* Regresa una lista con los campos que no estan el la lista dada */
//=============================================================================
List fieldsNoEquals(List Header, Row Data){
    List LIST = emptyList();
    List BufferList = emptyList();
    Row  BufferRow  = Data;
    int j = 0, i = 0;

    while(BufferRow != emptyRow()){
        BufferList = Header;
        while(BufferList != emptyList()){
            if(strcmp(headOfListData(BufferList),headOfRowName(BufferRow))) i++;
            BufferList = leftOfList(BufferList);
        }
        if(i>=listSize(Header)) LIST = addValue(LIST,j++,headOfRowName(BufferRow));
        i = 0;
        BufferRow = leftOfRow(BufferRow);
    }
    return LIST;
}

/* Carga la los campos seleccionados en una tabla, en caso de que sea vacia la lista regresa todos */
//=============================================================================
Table selectValues(const char *DBName, const char *TableName, List Fields){
    if(Fields == emptyList()) return LoadDB(DBName,TableName);

    Table DBTable = LoadDB(DBName,TableName);
    List FieldsR = fieldsNoEquals(Fields,headOfTableRow(DBTable));

    Table BufferTable = emptyTable();
    List BufferList;
    Row BufferRow;
    int i = 0;

    while(DBTable != emptyTable()){
        BufferRow = headOfTableRow(DBTable);
        BufferList = FieldsR;
        while(BufferList != emptyList()){
            BufferRow = removeField(BufferRow,headOfListData(BufferList));
            BufferList = leftOfList(BufferList);
        }
        BufferTable = addRow(BufferTable,i,BufferRow);
        DBTable = leftOfTable(DBTable);
    }

    while (BufferTable != emptyTable()) {
        DBTable = addRow(DBTable,i,headOfTableRow(BufferTable));
        BufferTable = leftOfTable(BufferTable);
    }

    return DBTable;
}

/* Elimina todos los registros que tengan, en el campo dado, el valor especificado */
//=============================================================================
int deleteValues(const char *DBName, const char *TableName, const char *FieldName, const char *Value)
{
    if(!existObject(getDirDB(DBName)) || !existObject(getDirTable(DBName, TableName)))
        return 0;

    int num_cols = getNumCols(DBName, TableName);
    char **fields = getFields(DBName, TableName);
    int num_field = 0, changes = 0;
    for(int i=0; i<num_cols; i++){
        if(strcmp(fields[i], FieldName)==0){
            num_field = i+1;
            break;
        }
    }
    /* No encontró el campo */
    if(!num_field)  return 0;
    num_field = num_field - 1;

    /* Abrir descriptor */
    FILE *Table = fopen(getDirTable(DBName, TableName), "r");
    FILE *Temp = fopen("temporal.txt", "w");

    char line[500];

    /* Copiar encabezado */
    for(int i=0; i<3; i++){
        fgets(line, sizeof(line), Table);
        fprintf(Temp, "%s", line);
    }

    while(fgets(line, sizeof(line), Table)){
        if(isAMatch(num_field, Value, line)){
            changes++;
        }
        else{
            fprintf(Temp, "%s", line);
        }
    }
    fclose(Table); fclose(Temp);
    remove(getDirTable(DBName, TableName));
    rename("temporal.txt", getDirTable(DBName, TableName));
    changeNumRows(DBName, TableName, -changes);
    return 1;
}

/* Revisa si la fila, en el campo especificado, tiene el valor dado */
//=============================================================================
int isAMatch(int num_field, const char* value, char *row)
{
    char *tok;
    char *aux = (char*) malloc(sizeof(char) * (strlen(row) + 1));
    strcpy(aux, row);
    tok = strtok(aux, "\t\t");
    for(int i=0; i<num_field; i++)
        tok = strtok(NULL, "\t\t");

    if (strcmp(value, tok) == 0){
        free(aux);
        return 1;
    }
    free(aux);
    return 0;
}
//=============================================================================
