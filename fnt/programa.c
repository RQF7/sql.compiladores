/*
 *  programa.c
 *  Implementación del módulo de programa de intérprete SQL.
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano,
 *  Marco Rubio Cortés.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/programa.h"
#include "include/sql.h"
#include "include/simbolo.h"
#include "include/analizador_sintactico.tab.h"
#include "include/operaciones_comunes.h"
#include "include/operaciones_definicion.h"
#include "include/operaciones_manipulacion.h"
#include "include/lista.h"
#include "include/table_struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <curses.h>

#define TAM_PILA 256
static Dato_de_pila pila[TAM_PILA];
static Dato_de_pila *apuntador_de_pila;
static char cadena_tmp[50];
static char *tabla_seleccionada;

#define TAM_PROGRAMA 5000
Instruccion programa[TAM_PROGRAMA];
Instruccion *apuntador_de_programa;
Instruccion *contador_de_programa;

void iniciar_codificacion()
{
    apuntador_de_programa = programa;
    apuntador_de_pila = pila;
}

Dato_de_pila sacar_de_pila()
{
    if (apuntador_de_pila <= pila)
        error_en_ejecucion("Apuntador de pila fuera de rango (por debajo)",
			(char *) 0);
    return *--apuntador_de_pila;
}

void meter_a_pila(Dato_de_pila d)
{
    if (apuntador_de_pila >= &pila[TAM_PILA])
        error_en_ejecucion("Apuntador de pila fuera de rango (por arriba)",
			(char *) 0);
    *apuntador_de_pila++ = d;
}

Instruccion *codificar(int n, ...)
{
	va_list valist;
	va_start(valist, n);
	Instruccion *pos_actual = 0;
	for (int i=0; i<n; i++) {
	    pos_actual = apuntador_de_programa;
	    if (apuntador_de_programa >= &programa[TAM_PROGRAMA])
	        error_en_ejecucion("Memoria ram saturada", (char *) 0);
	    *apuntador_de_programa++ = va_arg(valist, Instruccion);
	}
    return pos_actual;
}

void ejecutar(Instruccion *instruccion)
{
    for (contador_de_programa = instruccion; *contador_de_programa != DETENER;)
    	(*(*contador_de_programa++))();
}

/* Instrucciones de programa *************************************************/

void crear_base ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: crear_base\n");
		wrefresh(interprete);
	}

	int estado = newDataBase(((Simbolo *)(*contador_de_programa))->nombre);
	if (estado == 0) {
		sprintf(cadena_tmp, "La base '%s' ya existe.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_fallo(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "Base '%s' creada con éxito.\n\n",
			(((Simbolo *)(*contador_de_programa))->nombre));
		mensaje_exito(cadena_tmp);
		bases_crear(((Simbolo *)(*contador_de_programa++))->nombre);
	}
}

void borrar_base ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: borrar_base\n");
		wrefresh(interprete);
	}

	char *base = ((Simbolo *)(*contador_de_programa))->nombre;
	if(deleteDataBase(base)) {
		sprintf(cadena_tmp, "Base '%s' borrada con éxito.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_exito(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "La base '%s' no existe.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_fallo(cadena_tmp);
		return;
	}

	if (base_seleccionada != NULL)
		if (!strcmp(base_seleccionada, base)){
			base_seleccionada = NULL;
			bases_limpiar_seleccion();
		}
	bases_borrar(base);
}

void renombrar_base ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: renombrar_base\n");
		wrefresh(interprete);
	}

	char *original = ((Simbolo *)(*contador_de_programa++))->nombre;
	char *nuevo = ((Simbolo *)(*contador_de_programa++))->nombre;
	if (!renameDataBase(original, nuevo)) {
		sprintf(cadena_tmp, "La base '%s' no existe.\n\n", original);
		mensaje_fallo(cadena_tmp);
		return;
	}

	sprintf(cadena_tmp, "Base '%s' renombrada a '%s'.\n\n",
		original, nuevo);
	mensaje_exito(cadena_tmp);
	bases_renombrar(original, nuevo);

	if (base_seleccionada != NULL)
		if (!strcmp(base_seleccionada, original)){
			base_seleccionada = strdup(nuevo);
			bases_seleccionar();
		}

}

void seleccionar_base ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: seleccionar_base\n");
		wrefresh(interprete);
	}

	if (existObject(getDirDB(((Simbolo *)(*contador_de_programa))->nombre))) {
		base_seleccionada = strdup((\
			(Simbolo *)(*contador_de_programa++))->nombre);
		mensaje_exito("Ok.\n\n");
		bases_seleccionar();
	} else {
		sprintf(cadena_tmp, "La base '%s' no existe.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_fallo(cadena_tmp);
	}
}

void crear_tabla ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: crear_tabla\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL) {
		 error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);
		/* TODO: Vaciar pila. */
	}

	Dato_de_pila dato;
	List lista = emptyList();
	char *nombre = ((Simbolo *)(*contador_de_programa))->nombre;
	int n_args = (long int) contador_de_programa[1];
	contador_de_programa += 2;
	for(int i=0; i<n_args; i++) {
		dato = sacar_de_pila();
		lista = addValue(lista, i, dato.simbolo->nombre);
	}

	if(!newTable(base_seleccionada, nombre, lista)) {
		sprintf(cadena_tmp, "La tabla '%s' ya existe.\n\n", nombre);
		mensaje_fallo(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "Tabla '%s' creada correctamente.\n\n", nombre);
		mensaje_exito(cadena_tmp);
	}

	/* TODO: liberar memoria de pila!! */

}

void borrar_tabla ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: borrar_tabla\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL) {
		 error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);
		/* TODO: Vaciar pila. */
	}

	if(deleteTable(base_seleccionada,
		((Simbolo *)(*contador_de_programa))->nombre)) {
		sprintf(cadena_tmp, "Tabla '%s' borrada con éxito.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_exito(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "La tabla '%s' no existe.\n\n",
			(((Simbolo *)(*contador_de_programa++))->nombre));
		mensaje_fallo(cadena_tmp);
	}
}

void renombrar_tabla ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: renombrar_tabla\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		 error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	char *original = ((Simbolo *)(*contador_de_programa++))->nombre;
	char *nuevo = ((Simbolo *)(*contador_de_programa++))->nombre;
	if (!renameTable(base_seleccionada, original, nuevo)) {
		sprintf(cadena_tmp, "La tabla '%s' no existe.\n\n", original);
		mensaje_fallo(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "Tabla '%s' renombrada a '%s'.\n\n",
			original, nuevo);
		mensaje_exito(cadena_tmp);
	}
}

void codigo_alter ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: codigo_alter\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		 error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	tabla_seleccionada = strdup(((Simbolo *)
		(*contador_de_programa++))->nombre);
}

void agregar_campo ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: agregar_campo\n");
		wrefresh(interprete);
	}

	char *campo = ((Simbolo *)(*contador_de_programa++))->nombre;
	if(!newField(base_seleccionada, tabla_seleccionada, campo)) {
		sprintf(cadena_tmp, "La tabla '%s' no existe.\n\n", tabla_seleccionada);
		mensaje_fallo(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "Campo '%s' agregado a '%s'.\n\n",
			campo, tabla_seleccionada);
		mensaje_exito(cadena_tmp);
	}
}

void borrar_campo ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: borrar_campo\n");
		wrefresh(interprete);
	}

	char *campo = ((Simbolo *)(*contador_de_programa++))->nombre;
	if(!deleteField(base_seleccionada, tabla_seleccionada, campo)) {
		sprintf(cadena_tmp, "La tabla '%s' no existe.\n\n", tabla_seleccionada);
		mensaje_fallo(cadena_tmp);
	} else {
		sprintf(cadena_tmp, "Campo '%s'borrado.\n\n", campo);
		mensaje_exito(cadena_tmp);
	}
}

void renombrar_campo ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: agregar_campo\n");
		wrefresh(interprete);
	}

	char *original = ((Simbolo *)(*contador_de_programa++))->nombre;
	char *nuevo = ((Simbolo *)(*contador_de_programa++))->nombre;

	if(!renameField(base_seleccionada, tabla_seleccionada, original, nuevo)) {
		sprintf(cadena_tmp, "La tabla '%s' no existe.\n\n", tabla_seleccionada);
		mensaje_fallo(cadena_tmp);
		/* TODO: códigos de errores */
	} else {
		sprintf(cadena_tmp, "Campo '%s' renombrado a '%s'.\n\n",
			original, nuevo);
		mensaje_exito(cadena_tmp);
	}
}

void mostrar_bases ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: mostrar_bases\n");
		wrefresh(interprete);
	}

	imprimir_lista(showDatabases(), "Bases de datos");
}

void mostrar_tablas ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: mostrar_tablas\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	imprimir_lista(showTables(base_seleccionada), "Tablas");
}

void mostrar_columnas ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: mostrar_columnas\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);
	char *tabla = ((Simbolo *)(*contador_de_programa++))->nombre;
	imprimir_lista(showFields(base_seleccionada, tabla), "Campos");
}

void meter_cadena ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: meter_cadena\n");
		wrefresh(interprete);
	}

	Dato_de_pila dato;
	dato.simbolo = (Simbolo *)(*contador_de_programa++);
	meter_a_pila(dato);
}

void meter_entero ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: meter_entero\n");
		wrefresh(interprete);
	}

	Dato_de_pila dato;
	dato.valor = (long int)(*contador_de_programa++);
	meter_a_pila(dato);
}

void codigo_select_simple ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: codigo_insert_simple\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	Dato_de_pila dato;
	List lista = emptyList();
	char *nombre = ((Simbolo *)(*contador_de_programa))->nombre;
	int n_args = (long int) contador_de_programa[1];
	contador_de_programa += 2;
	for(int i=0; i<n_args; i++) {
		dato = sacar_de_pila();
		lista = addValue(lista, i, dato.simbolo->nombre);
	}

	imprimir_tabla(selectValues(base_seleccionada, nombre, lista));
}

void codigo_insert_simple ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: codigo_insert_simple\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	Dato_de_pila dato;
	List lista = emptyList();
	char *nombre = ((Simbolo *)(*contador_de_programa))->nombre;
	int n_args = (long int) contador_de_programa[1];
	contador_de_programa += 2;
	for(int i=0; i<n_args; i++) {
		dato = sacar_de_pila();
		lista = addValue(lista, i, dato.simbolo->nombre);
	}

	if (insertValues(base_seleccionada, nombre, lista))
		mensaje_exito("Inserción exitosa.\n\n");
	else
		mensaje_fallo("Error en operación de inserción.\n\n");

}

void codigo_insert_compuesto ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: codigo_insert_compuesto\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	Dato_de_pila dato;
	char *nombre = ((Simbolo *)(*contador_de_programa))->nombre;
	int n_args = (long int) contador_de_programa[1];
	contador_de_programa += 2;

	for(int i=0; i<n_args; i++) {

		List lista = emptyList();
		dato = sacar_de_pila();
		int n_args2 = dato.valor;
		for (int j=0; j<n_args2; j++) {
			dato = sacar_de_pila();
			lista = addValue(lista, j, dato.simbolo->nombre);
		}

		if (!insertValues(base_seleccionada, nombre, lista)) {
			mensaje_fallo("Error en operación de inserción.\n\n");
			return;
		}

	}

	mensaje_exito("Inserción exitosa.\n\n");
}

void codigo_delete ()
{
	if (DEBUG) {
		wprintw(interprete, "DEBUG: codigo_delete\n");
		wrefresh(interprete);
	}

	if (base_seleccionada == NULL)
		error_en_ejecucion("No hay ninguna base seleccionada", (char *) 0);

	char *tabla = ((Simbolo *)(*contador_de_programa++))->nombre;
	char *campo = ((Simbolo *)(*contador_de_programa++))->nombre;
	Dato_de_pila dato = sacar_de_pila();

	deleteValues(base_seleccionada, tabla, campo, dato.simbolo->nombre);
	mensaje_exito("Borrado exitoso.\n\n");
}
