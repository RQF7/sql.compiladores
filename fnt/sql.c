/*
 *  sql.c
 *  Método main de intérprete.
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano,
 *  Marco Rubio Cortés.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "include/vector_str.h"
#include "include/programa.h"
#include "include/simbolo.h"
#include "include/sql.h"
#include "include/lista.h"
#include "include/operaciones_definicion.h"
#include "include/analizador_sintactico.tab.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <locale.h>
#include <signal.h>
#include <setjmp.h>

void iniciar_ncurses ();
void crear_panel_estado (int x, int y);
void crear_panel_interprete (int x, int y);

/* Variables semi-globales */
static jmp_buf punto_de_inicio;
static char* nombre_programa;

int main (int argc, char *argv[])
{
	iniciar_ncurses();
	signal(SIGUSR1, error_sql);
	nombre_programa = argv[0];
	numero_de_linea = 1;
	historial = 0;
	wprintw(interprete, "sql> ");
	wrefresh(interprete);
	setjmp(punto_de_inicio);
	for(iniciar_codificacion(); yyparse(); iniciar_codificacion()){
		ejecutar(programa);
	}
	delwin(panel);
	delwin(interprete);
	endwin();
	free(linea);
	liberar_vector_str(historial);
	return 0;
}

void iniciar_ncurses ()
{
	/* Configuración global. */
	int x, y;
	setlocale(LC_ALL, "");
	initscr();
	getmaxyx(stdscr, y, x);
	raw();
	keypad(stdscr, TRUE);
	idlok(stdscr, TRUE);
	wsetscrreg(stdscr, 0, y);
	scrollok(stdscr, TRUE);
	noecho();

	/* Colores de estado. */
	start_color();
	use_default_colors();
	init_pair(1, COLOR_GREEN, -1);
	init_pair(2, COLOR_RED, -1);
	linea = malloc(sizeof(int) * x);

	/* Mensaje de bienvenida. */
	char *lin1 = "Gestor de Bases de Datos - Intérprete";
	char *lin2a = "Daniel Ayala Zamorano";
	char *lin2b = "Laura Natalia Borbolla Palacios";
	char *lin2c = "Ricardo Quezada Figueroa";
	char *lin3 = "Compiladores";
	char *lin4 = "ESCOM - IPN";
	char *lin5 = "Presiona <Enter> para iniciar";
	attron(A_BOLD);
	attron(COLOR_PAIR(1));
	mvprintw(y/2 - 4, (x - strlen(lin1)) / 2, "%s", lin1);
	attroff(COLOR_PAIR(1));
	attroff(A_BOLD);
	mvprintw(y/2 - 2, (x - strlen(lin2a)) / 2, "%s", lin2a);
	mvprintw(y/2 - 1, (x - strlen(lin2b)) / 2, "%s", lin2b);
	mvprintw(y/2, (x - strlen(lin2c)) / 2, "%s", lin2c);
	mvprintw(y/2 + 2, (x - strlen(lin3)) / 2, "%s", lin3);
	mvprintw(y/2 + 3, (x - strlen(lin4)) / 2, "%s", lin4);
	mvprintw(y - 1, (x - strlen(lin5)) / 2, "%s", lin5);
	refresh();
	getch();
	clear();
	refresh();

	/* Ventanas secundarias */
	crear_panel_estado (x, y);
	crear_panel_interprete (x, y);
}

void crear_panel_interprete (int x, int y)
{
	interprete = subwin(stdscr, y - 2, 3 * x / 4 - 2, 1, 1);
	keypad(interprete, TRUE);
	idlok(interprete, TRUE);
	wsetscrreg(interprete, 0, y-1);
	scrollok(interprete, TRUE);
	wborder(interprete, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wmove(interprete, 0, 0);
	wrefresh(interprete);
}

void crear_panel_estado (int x, int y)
{
	panel = subwin(stdscr, y - 1, x/4, 1, 3 * x / 4);
	wborder(panel, 0, ' ', ' ', ' ', ACS_VLINE, ' ', ' ', ' ');
	wrefresh(panel);

	getmaxyx(panel, y, x);
	char *lin1 = "Intérprete SQL";
	char *lin2 = "Gestor de Bases de Datos";
	char *lin3 = "ESCOM - IPN";

	mvwprintw(panel, 2, (x - strlen(lin1)) / 2, "%s", lin1);
	mvwprintw(panel, 3, (x - strlen(lin2)) / 2, "%s", lin2);
	mvwprintw(panel, 4, (x - strlen(lin3)) / 2, "%s", lin3);
	wmove(panel, 6, 2);
	for(int i=0; i<x-2; i++)
		waddch(panel, ACS_HLINE);

	char *lin4 = "BASES DE DATOS: ";
	mvwprintw(panel, 8, 2, "%s", lin4);
	List bases = showDatabases();
	int indice = 0;
	while(bases != NULL) {
		mvwprintw(panel, 10 + indice, 4, "[ ] %s", bases->dataX);
		bases = bases->next;
		indice++;
	}

	wrefresh(panel);
}

void warning (char *mensaje, char *detalle)
{
        wattron(interprete, COLOR_PAIR(2));
        wprintw(interprete, "%s: %s.\n", nombre_programa, mensaje);
        if (detalle)
                wprintw(interprete, " %s. ", detalle);
        wprintw(interprete, "Cerca de la línea %d.\n\n", numero_de_linea);
        wattroff(interprete, COLOR_PAIR(2));
        wrefresh(interprete);
}

void error_en_ejecucion (char *mensaje, char *detalle)
{
        warning(mensaje, detalle);
        longjmp(punto_de_inicio, 0);
}

void error_sql ()
{
    //error_en_ejecucion("Error en sql", sql_ERROR);
}

void prompt ()
{
	if (base_seleccionada == NULL)
		wprintw(interprete, "sql> ");
	else
		wprintw(interprete, "sql [%s]> ", base_seleccionada);
	wrefresh(interprete);
}

void mensaje_exito (char *mensaje)
{
	wattron(interprete, COLOR_PAIR(1));
	wprintw(interprete, mensaje);
	wattroff(interprete, COLOR_PAIR(1));
	wrefresh(interprete);
}

void mensaje_fallo (char *mensaje)
{
	wattron(interprete, COLOR_PAIR(2));
	wprintw(interprete, mensaje);
	wattroff(interprete, COLOR_PAIR(2));
	wrefresh(interprete);
}

int bases_buscar (char *nombre)
{
	int fila = 10;
	while ((mvwinch(panel, fila, 8) & A_CHARTEXT) != ' ') {
		//wprintw(panel, "L");
		int columna = 0;
		while (nombre[columna] ==
			(mvwinch(panel, fila, 8 + columna) & A_CHARTEXT))
			columna++;
		if (columna == strlen(nombre))
			return fila;
		fila++;
	}
	return -1;
}

void bases_seleccionar ()
{
	bases_limpiar_seleccion();
	int fila = bases_buscar(base_seleccionada);
	mvwprintw(panel, fila, 5, "*");
	wrefresh(panel);
}

void bases_crear (char *base)
{
	int fila = 10;
	while ((mvwinch(panel, fila, 8) & A_CHARTEXT) != ' ')
		fila++;
	winsertln(panel);
	wborder(panel, 0, ' ', ' ', ' ', ACS_VLINE, ' ', ' ', ' ');
	mvwprintw(panel, fila, 4, "[ ] %s", base);
	wrefresh(panel);
}

void bases_borrar (char *base)
{
	bases_buscar(base);
	wdeleteln(panel);
	wrefresh(panel);
}

void bases_limpiar_seleccion ()
{
	int fila = 10;
	while ((mvwinch(panel, fila, 8) & A_CHARTEXT) != ' ') {
		mvwprintw(panel, fila, 5, " ");
		fila++;
	}
	wrefresh(panel);
}

void bases_renombrar (char *origen, char *destino)
{
	int fila = bases_buscar(origen);
	mvwprintw(panel, fila, 4, "[ ] %s", destino);
	wclrtoeol(panel);
	wrefresh(panel);
}

void imprimir_lista (List lista, char *encabezado)
{
	int max_y;
	int x, y, max = strlen(encabezado), indice = 3;
	getmaxyx(interprete, max_y, x);
	getyx(interprete, y, x);

	List copia = lista;
	int contador = 0;
	while (copia != NULL) {
		contador++;
		copia = copia->next;
	}
	contador += 5;
	if (y + contador > max_y){
		wscrl(interprete, y + contador - max_y);
		getyx(interprete, y, x);
		y -= y + contador - max_y;
	}

	mvwprintw(interprete, y + 1, x + 2, "%s", encabezado);
	while(lista != NULL) {
		mvwprintw(interprete, y + indice, x + 2, "%s", lista->dataX);
		if(strlen(lista->dataX) > max)
			max = strlen(lista->dataX);
		lista = lista->next;
		indice++;
	}

	wmove(interprete, y, x);
	waddch(interprete, ACS_ULCORNER);
	wmove(interprete, y, x + max + 3);
	waddch(interprete, ACS_URCORNER);

	wmove(interprete, y + indice, x);
	waddch(interprete, ACS_LLCORNER);
	wmove(interprete, y + indice, x + max + 3);
	waddch(interprete, ACS_LRCORNER);

	for(int i=0; i<indice - 1; i++) {
		wmove(interprete, y + 1 + i, x);
		waddch(interprete, ACS_VLINE);
	}

	for(int i=0; i<indice - 1; i++) {
		wmove(interprete, y + 1 + i, max + 3);
		waddch(interprete, ACS_VLINE);
	}

	wmove(interprete, y + 2, x);
	waddch(interprete, ACS_LTEE);
	wmove(interprete, y + 2, x + max + 3);
	waddch(interprete, ACS_RTEE);

	wmove(interprete, y, x + 1);
	for(int i=0; i<max + 2; i++)
		waddch(interprete, ACS_HLINE);

	wmove(interprete, y + 2, x + 1);
	for(int i=0; i<max + 2; i++)
		waddch(interprete, ACS_HLINE);

	wmove(interprete, y + indice, x + 1);
	for(int i=0; i<max + 2; i++)
		waddch(interprete, ACS_HLINE);

	wmove(interprete, y + indice + 1, x);
	wprintw(interprete, "\n");
	wrefresh(interprete);
}

void imprimir_tabla (Table tabla)
{
	int max_y;
	int x, y;
	getmaxyx(interprete, max_y, x);
	getyx(interprete, y, x);

	Table copia = tabla;
	int contador = 0;
	while (copia != NULL) {
		contador++;
		copia = copia->Next;
	}
	contador += 5;
	if (y + contador > max_y){
		wscrl(interprete, y + contador - max_y);
		getyx(interprete, y, x);
		y -= y + contador - max_y;
	}

	for(int i=0; i<contador - 3; i++) {
		wmove(interprete, y + 1 + i, x);
		waddch(interprete, ACS_VLINE);
	}

	wmove(interprete, y, x);
	waddch(interprete, ACS_ULCORNER);

	wmove(interprete, y + contador - 2, x);
	waddch(interprete, ACS_LLCORNER);

	wmove(interprete, y + 2, x);
	waddch(interprete, ACS_LTEE);

	wmove(interprete, y, x);

	/* Para cada columna */
	int contador_columnas = 0, contador_filas;
	int desplazamiento = 2;
	int bandera_ciclo = 1;
	while(bandera_ciclo){

		copia = tabla;
		contador_filas = 0;
		int max = 0;

		/* Para cada fila */
		while (copia != NULL) {
			Row fila = copia->Row;
			for (int i=0; i<contador_columnas; i++)
				fila = fila->Next;

			if (fila->Next == NULL)
				bandera_ciclo = 0;

			if (contador_filas == 0) {
				mvwprintw(interprete, y + 1, x + desplazamiento,
					"%s", fila->Name);
				max = strlen(fila->Name);
				mvwprintw(interprete, y + 3, x + desplazamiento,
					"%s", fila->Info);
				if (strlen(fila->Info) > max)
					max = strlen(fila->Info);
			} else {
				mvwprintw(interprete, y + 3 + contador_filas,
					x + desplazamiento, "%s", fila->Info);
				if (strlen(fila->Info) > max)
					max = strlen(fila->Info);
			}

			contador_filas++;
			copia = copia->Next;
		}

		for(int i=0; i<contador - 3; i++) {
			wmove(interprete, y + 1 + i, x + desplazamiento + max + 1);
			waddch(interprete, ACS_VLINE);
		}

		wmove(interprete, y, x + desplazamiento - 1);
		for(int i=x + desplazamiento + 1; i<x + desplazamiento + max + 3; i++)
			waddch(interprete, ACS_HLINE);

		wmove(interprete, y + 2, x + desplazamiento - 1);
		for(int i=x + desplazamiento + 1; i<x + desplazamiento + max + 3; i++)
			waddch(interprete, ACS_HLINE);

		wmove(interprete, y + contador - 2, x + desplazamiento - 1);
		for(int i=x + desplazamiento + 1; i<x + desplazamiento + max + 3; i++)
			waddch(interprete, ACS_HLINE);

		wmove(interprete, y, x + desplazamiento + max + 1);
		waddch(interprete, (bandera_ciclo) ? ACS_TTEE : ACS_URCORNER);

		wmove(interprete, y + 2, x + desplazamiento + max + 1);
		waddch(interprete, (bandera_ciclo) ? ACS_PLUS : ACS_RTEE);

		wmove(interprete, y + contador - 2, x + desplazamiento + max + 1);
		waddch(interprete, (bandera_ciclo) ? ACS_BTEE : ACS_LRCORNER);

		desplazamiento += max + 3;
		contador_columnas++;
	}

	wmove(interprete, y + 3 + 1 + contador_filas, x);
	wprintw(interprete, "\n");
	wrefresh(interprete);
}
