#include "include/operaciones_comunes.h"

/* Verifica si un archivo o carpeta ya existe, Si(1) No(0)*/
//=============================================================================
int existObject(const char *ObjectName)
{
    struct stat st = {0};
    if(stat(ObjectName,&st) != -1)  return 1;
    else                            return 0;
}

/* Regresa la direccion del descriptor de una base de datos */
//=============================================================================
const char *getDescriptorName(const char *DBName)
{
    char *descriptor_file_name;
    descriptor_file_name = (char*) malloc( (sizeof(const char) * strlen(DBName) * 2) + 30 + (sizeof(const char) * strlen(DB_FOLDER) ) );
    strcpy(descriptor_file_name, DB_FOLDER);
    strcat(descriptor_file_name,"/");
    strcat(descriptor_file_name,DBName);
    strcat(descriptor_file_name,"/DB_Descriptor_");
    strcat(descriptor_file_name,DBName);
    strcat(descriptor_file_name, ".txt");
    return descriptor_file_name;
}

/* Regresa la direccion de una tabla de una base de datos */
//=============================================================================
const char *getDirTable(const char *DBName, const char *TableName)
{
    char *dir_table;
    dir_table =  (char*) (char*) malloc(sizeof(char)*(strlen(DB_FOLDER)+strlen(DBName)+strlen(TableName)+10));
    strcpy(dir_table, DB_FOLDER);
    strcat(dir_table,"/");
    strcat(dir_table,DBName);
    strcat(dir_table,"/");
    strcat(dir_table,TableName);
    strcat(dir_table, ".txt");
    return dir_table;
}

/* Regresa la direccion de una base de datos */
//=============================================================================
const char *getDirDB(const char *DBName)
{
    char *dir_DB;
    dir_DB =  (char*) (char*) malloc(sizeof(char)*(strlen(DB_FOLDER)+strlen(DBName)+5));
    strcpy(dir_DB, DB_FOLDER);
    strcat(dir_DB,"/");
    strcat(dir_DB,DBName);
    return dir_DB;
}

/* Retorna un atributo de una tabla */
//=============================================================================
int getDBAttibute(const char *DBName, const char *TableName, int Attr)
{
    if(!existObject(getDirTable(DBName,TableName)))  return -2;

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptor = fopen(getDescriptorName(DBName),"r+");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(table_label,line)){
            for(int i=0; i<Attr; i++)
                fgets(line, sizeof(line), Descriptor);
            strtok(line, " ");
            return atoi(line);
        }
    }
    fclose(Descriptor);
    return -1;
}

/* Retorna el numero de columnas de una tabla */
//=============================================================================
int getNumCols(const char *DBName, const char *TableName)
{
    return getDBAttibute(DBName,TableName,1);
}

/* Retorna el numero de filas de una tabla */
//=============================================================================
int getNumRows(const char *DBName, const char *TableName)
{
    return getDBAttibute(DBName,TableName,2);
}

/* Regresa un arreglo bidimensional con los campos de una tabla */
//=============================================================================
char** getFields(const char *DBName, const char *TableName)
{
    if(!existObject(getDirTable(DBName, TableName)))    return 0;
    FILE *Descriptor = fopen(getDescriptorName(DBName), "r");

    int numCols = getNumCols(DBName, TableName);

    int r = numCols, c = 500;
    char **fields = (char **)malloc(r * sizeof(char *));
    for (int i=0; i<r; i++)
        fields[i] = (char *)malloc(c * sizeof(char));

    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptor)) {
        if(!strcmp(table_label, line)){
            for(int i=0; i<3; i++){
                fgets(line, sizeof(line), Descriptor);
            }
            for(int i=0; i<numCols; i++){
                fgets(line, sizeof(line), Descriptor);
                char* campo = removeTabs(line);
                strcpy(fields[i], campo);
                free(campo);
            }
        }
    }
    fclose(Descriptor);
    return fields;
}

/* Incrementa el numero de filas de una tabla en el descriptor de la BD */
//=============================================================================
int incNumRows(const char *DBName, const char *TableName)
{
    if(!existObject(getDirTable(DBName,TableName)))  return 0;

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptorv1 = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("bffr.bffr","w");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptorv1)) {
        if(!strcmp(table_label,line)){
            for(int i=0; i<2; i++){
                fprintf(Descriptorv2, "%s",line);
                fgets(line, sizeof(line), Descriptorv1);
            }
            strtok(line, " ");
            fprintf(Descriptorv2, "%d Rows\n",atoi(line)+1);
            strcpy(line,"");
        }fprintf(Descriptorv2, "%s",line);
    }

    free(table_label);
    fclose(Descriptorv1); fclose(Descriptorv2);
    rename("bffr.bffr",getDescriptorName(DBName));
    return 1;
}

/* Incrementa el numero de filas de una tabla en el descriptor de la BD */
//=============================================================================
int changeNumRows(const char *DBName, const char *TableName, int change)
{
    if(!existObject(getDirTable(DBName,TableName)))  return 0;

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptorv1 = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("bffr.bffr","w");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptorv1)) {
        if(!strcmp(table_label,line)){
            for(int i=0; i<2; i++){
                fprintf(Descriptorv2, "%s",line);
                fgets(line, sizeof(line), Descriptorv1);
            }
            strtok(line, " ");
            fprintf(Descriptorv2, "%d Rows\n",atoi(line)+change);
            strcpy(line,"");
        }fprintf(Descriptorv2, "%s",line);
    }

    free(table_label);
    fclose(Descriptorv1); fclose(Descriptorv2);
    rename("bffr.bffr",getDescriptorName(DBName));
    return 1;
}

/* Incrementa el numero de columnas de una tabla en el descriptor de la BD */
//=============================================================================
int changeNumCols(const char *DBName, const char *TableName, int change)
{
    if(!existObject(getDirTable(DBName,TableName)))  return 0;

    /* Se abre el descriptor y se crea la tabla como archivo */
    FILE *Descriptorv1 = fopen(getDescriptorName(DBName),"r");
    FILE *Descriptorv2 = fopen("bffr.bffr","w");
    char line[500];
    char *table_label;
    table_label   = (char*) malloc(sizeof(char) * strlen(TableName)+10);
    strcpy(table_label,"TABLE :: ");
    strcat(table_label,TableName);
    strcat(table_label,"\n");

    while (fgets(line, sizeof(line), Descriptorv1)) {
        if(!strcmp(table_label,line)){
            fprintf(Descriptorv2, "%s",line);
            fgets(line, sizeof(line), Descriptorv1);
            strtok(line, " ");
            fprintf(Descriptorv2, "%d Columns\n",atoi(line)+change);
            strcpy(line,"");
        }fprintf(Descriptorv2, "%s",line);
    }

    fclose(Descriptorv1); fclose(Descriptorv2);
    rename("bffr.bffr",getDescriptorName(DBName));
    return 1;
}

/* Elimina los tabuladores y saltos de línea */
//=============================================================================
char* removeTabs(char *string)
{
    int longitud = strlen(string), j=0;
    char* string2 = (char*) malloc (sizeof(char)*longitud);
    for(int i=0; i<longitud; i++){
        if(string[i]!='\t' && string[i]!='\n'){
            string2[j]=string[i];
            j++;
        }
    }
    string2[j]='\0';
    return string2;
}
/* Elimina saltos de linea */
//=============================================================================
char* removeJumps(char *string)
{
    int longitud = strlen(string), j=0;
    char* string2 = (char*) malloc (sizeof(char)*longitud);
    for(int i=0; i<longitud; i++){
        if(string[i]!='\n'){
            string2[j]=string[i];
            j++;
        }
    }
    string2[j]='\0';
    return string2;
}

/* Verifica si una cadena termina con un sufijo dado */
//=============================================================================
int string_ends_with(const char * str, const char * suffix)
{
  int str_len = strlen(str);
  int suffix_len = strlen(suffix);

  return
    (str_len >= suffix_len) &&
    (0 == strcmp(str + (str_len-suffix_len), suffix));
}

/* Verifica si una cadena comienza con un prefijo dado */
//=============================================================================
int string_starts_with(const char* string, const char* prefix)
{
    while(*prefix)
    {
        if(*prefix++ != *string++)
            return 0;
    }

    return 1;
}
//=============================================================================
