/*
 *  analizador_sintactico.y
 *  Analizador léxico de SQL
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano,
 *  Marco Rubio Cortés.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%{

	#include "include/vector_str.h"
	#include "include/programa.h"
	#include "include/simbolo.h"
	#include "include/sql.h"
	#include "include/analizador_sintactico.tab.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <ctype.h>
	#include <curses.h>
	#include <locale.h>

	int filtro_entrada();
	void agregar_a_historial(int longitud);
	void borrar(int posicion);
	char *filtrar_cadena(char *cadena);
	int yywrap ();

	#define YY_INPUT(buf,result,max_size) \
	int contador = filtro_entrada(); \
	for(int i=0; i<contador; i++) \
	        buf[i] = linea[i]; \
	result = contador; \

%}

%%

 /* Espacios en blanco */
[ \t]									{ ; }
[\n]									{ if(dentro_de_definicion) {
											wprintw(interprete, "... ");
											wrefresh(interprete);
										  } else
										  	prompt();
										  numero_de_linea++; }

 /* Expresiones de salida */
"salir"                         		{ printw("Adiós!\n"); return 0; }
<<EOF>>                         		{ printw("\nAdiós!\n"); return 0; }

 /* Palabras reservadas */
"CREATE"|"create"|"Create"				{ return CREATE; }
"DROP"|"drop"|"Drop"					{ return DROP; }
"ALTER"|"alter"|"Alter"					{ return ALTER; }
"SHOW"|"show"|"Show"					{ return SHOW; }
"DATABASES"|"databases"|"Databases"		{ return DATABASES; }
"TABLES"|"tables"|"Tables"				{ return TABLES; }
"COLUMNS"|"columns"|"Columns"			{ return COLUMNS; }
"RENAME"|"rename"|"Rename"				{ return RENAME; }
"TO"|"to"|"To"							{ return TO; }
"USE"|"use"|"Use"						{ return USE; }
"DATABASE"|"database"|"Database"		{ return DATABASE; }
"TABLE"|"table"|"Table"					{ return TABLE; }
"COLUMN"|"column"|"Column"				{ return COLUMN; }
"PRIMARY"|"primary"|"Primary"			{ return PRIMARY; }
"KEY"|"key"|"Key"						{ return KEY; }
"FOREIGN"|"foreign"|"Foreign"			{ return FOREIGN; }
"REFERENCES"|"references"|"References" 	{ return REFERENCES; }
"ADD"|"add"|"Add"						{ return ADD; }
"MODIFY"|"modify"|"Modify"				{ return MODIFY; }
"CHANGE"|"change"|"Change"				{ return CHANGE; }
"INT"|"int"|"Int"						{ return INT; }
"DOUBLE"|"double"|"Double"				{ return DOUBLE; }
"CHAR"|"char"|"Char"					{ return CHAR; }
"VARCHAR"|"varchar"|"Varchar"			{ return VARCHAR; }
"SELECT"|"select"|"Select"				{ return SELECT; }
"FROM"|"from"|"From"					{ return FROM; }
"WHERE"|"where"|"Where"					{ return WHERE; }
"INSERT"|"insert"|"Insert"				{ return INSERT; }
"INTO"|"into"|"Into"					{ return INTO; }
"UPDATE"|"update"|"Update"				{ return UPDATE; }
"SET"|"set"|"Set"						{ return SET; }
"DELET"|"delete"|"Delete"				{ return DELETE; }
"VALUES"|"values"|"Values"				{ return VALUES; }
"DEFAULT"|"default"|"Default"			{ return DEFAULT; }
"IN"|"in"|"In"							{ return IN; }
"IS"|"is"|"Is"							{ return IS; }
"TRUE"|"true"|"True"					{ return BOL_TRUE; }
"FALSE"|"false"|"False"					{ return BOL_FALSE; }
"BETWEEN"|"between"|"Between"			{ return BETWEEN; }
"LIKE"|"like"|"Like"					{ return LIKE; }

 /* Operadores lógicos */
"OR"|"or"|"Or"|"||"						{ return OR; }
"AND"|"and"|"And"|"&&"					{ return AND; }
"XOR"|"xor"|"Xor"						{ return XOR; }
"NOT"|"not"|"Not"|"!"					{ return NOT; }

 /* Operadores relacionales */
">="									{ return MAIG; }
"<="									{ return MEIG; }
"!="									{ return DIST; }
"<<"									{ return CI; }
">>"									{ return CD; }

 /* Literales */
[0-9]+									{ return NUMERO; /* TODO: filtro */ }
[0-9]+\.?[0-9]*|\.[0-9]+				{ return NUMERO_FLOTANTE; }
\"(\\.|[^"])*\"							{ Simbolo *simbolo;
										  simbolo = instalar(
											 	filtrar_cadena(yytext), ID, 0);
										  yylval.simbolo = simbolo;
										  return CADENA; }
[A-Za-z][A-Za-z_0-9]*					{ Simbolo *simbolo;
										  if((simbolo = buscar(yytext)) == 0)
										  	simbolo = instalar(strdup(yytext),
												ID, 0);
										  yylval.simbolo = simbolo;
										  return ID; }

 /* Caulquier otro caracter */
.										{ return yytext[0]; }

%%

int filtro_entrada()
{
	int x, y;
	int contador = 0;
	getyx(interprete, y, x);
	int previos = x;
	Contenedor_str *iterador = NULL;

	if(historial != NULL)
		iterador = historial->primero;

	while(1) {

		getyx(interprete, y, x);
		int caracter = wgetch(interprete);

		if(caracter == KEY_F(5)) {
			wscrl(interprete, -1);
			continue;
		}

		if(caracter == KEY_F(6)) {
			scroll(interprete);
			continue;
		}

		if(caracter == KEY_LEFT){
			if(x == previos) continue;
			wmove(interprete, y, x - 1);
			wrefresh(interprete);
			continue;
		}

		if(caracter == KEY_RIGHT){
			if(x - previos == contador) continue;
			wmove(interprete, y, x + 1);
			wrefresh(interprete);
			continue;
		}

		if(caracter == KEY_UP){
			if(iterador == NULL) continue;
			for(int i=x; i>previos; i--) {
				wmove(interprete, y, i - 1);
				wdelch(interprete);
				wrefresh(interprete);
			}
			for(contador=0; contador<strlen(iterador->cadena); contador++) {
				linea[contador] = iterador->cadena[contador];
				waddch(interprete, linea[contador]);
				wrefresh(interprete);
			}
			if(iterador->siguiente != NULL)
				iterador = iterador->siguiente;
		}

		if(caracter == KEY_DOWN){
			for(int i=x; i>previos; i--) {
				wmove(interprete, y, i- 1);
				wdelch(interprete);
				wrefresh(interprete);
			}
			contador = 0;
			if(iterador == NULL) continue;
			if(iterador->anterior != NULL)
				iterador = iterador->anterior;
			else continue;
			for(contador=0; contador<strlen(iterador->cadena); contador++) {
				linea[contador] = iterador->cadena[contador];
				waddch(interprete, linea[contador]);
				wrefresh(interprete);
			}
		}

		if(caracter == KEY_BACKSPACE || caracter == 127){ //Borrar
			if(x == previos) continue;
			wmove(interprete, y, x - 1);
			contador--;
			borrar(x - previos - 1);
			wdelch(interprete);
			wrefresh(interprete);
			continue;
		}

		if(caracter == 9) { //TAB
			if(x - previos == contador){
				for(int i=0; i<8; i++) {
					linea[contador] = ' ';
					contador++;
					waddch(interprete, ' ');
					wrefresh(interprete);
				}
			}
		}

		if(isprint(caracter)) {

			if(x - previos == contador){
				linea[contador] = caracter;
				contador++;
				waddch(interprete, caracter);
				wrefresh(interprete);
			}else{
				int respaldo = x;
				contador++;
				while(x - previos != contador){
					int aux = linea[x - previos];
					linea[x - previos] = caracter;
					waddch(interprete, caracter);
					caracter = aux;
					x++;
				}
				wmove(interprete, y, respaldo + 1);
			}

		}

		if(caracter == '\n') {
			linea[contador] = caracter;
			contador++;
			if(x - previos == contador)
			waddch(interprete, caracter);
			else
			mvwaddch(interprete, y, previos + contador, caracter);
			if(contador > 1)
			agregar_a_historial(contador);
			return contador;
		}
	}
}

void agregar_a_historial(int longitud)
{
    char *nueva_cadena = malloc(sizeof(char) * longitud);
    for(int i=0; i<longitud-1; i++)
        nueva_cadena[i] = linea[i];
    nueva_cadena[longitud -1] = 0;
    if(historial == NULL)
        historial = nuevo_vector_str(nueva_cadena);
    else
        agregar_a_vector_str(historial, nueva_cadena);
}

void borrar(int posicion)
{
    int aux, num_elementos;
    getmaxyx(interprete, aux, num_elementos);
    for(int i = posicion; posicion < num_elementos - 1; posicion++)
        linea[posicion] = linea[posicion + 1];
}

char *filtrar_cadena(char *cadena)
{
	int lon_original = strlen(cadena);
	char *resultado = malloc(sizeof(char) * lon_original - 1);
	for(int i=0; i<lon_original - 2; i++)
		resultado[i] = cadena[i + 1];
	resultado[lon_original - 2] = 0;
	return resultado;
}

int yywrap ()
{
	return 1;
}
