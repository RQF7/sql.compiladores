/*
 *  analizador_sintactico.y
 *  Especificación de subconjunto de gramática SQL
 *
 *  Copyright (C) 2017 Ricardo Quezada Figueroa,
 *  Laura Natalia Borbolla Palacios,
 *  Daniel Ayala Zamorano.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%{

	#include "include/simbolo.h"
	#include "include/programa.h"
	#include "include/sql.h"
	#include <stdio.h>
	#include <stdlib.h>
	#include <curses.h>
	#include <locale.h>
	#include <signal.h>
	#include <setjmp.h>

	int yylex ();
	void yyerror (char *mensaje);

%}

%union {
	Simbolo *simbolo;
	Instruccion *instruccion;
	int num_elementos;
}

/* Tokens espejo (palabras reservadas): */
%token <simbolo> CREATE DROP ALTER RENAME TO USE
%token <simbolo> DATABASE TABLE COLUMN
%token <simbolo> PRIMARY KEY SHOW DATABASES TABLES
%token <simbolo> FOREIGN REFERENCES COLUMNS
%token <simbolo> ADD MODIFY CHANGE
%token <simbolo> INT DOUBLE CHAR VARCHAR
%token <simbolo> SELECT FROM WHERE INSERT INTO
%token <simbolo> UPDATE SET DELETE VALUES DEFAULT
%token <simbolo> IN IS BOL_TRUE BOL_FALSE BETWEEN LIKE

/* Tokens no espejo (literales y operadores): */
%token <simbolo> ID
%token <simbolo> NUMERO
%token <simbolo> NUMERO_FLOTANTE
%token <simbolo> CADENA
%token <simbolo> OR AND XOR NOT
%token <simbolo> MAIG MEIG DIST
%token <simbolo> CI CD

/* No terminales */
%type <instruccion> definicion manipulacion
%type <instruccion> create_database drop_database use_database create_table
%type <instruccion> def_creacion tipo_dato def_columna rename_database
%type <instruccion> drop_table rename_table alter_table alter
%type <instruccion> show_databases show_tables show_columns expresion
%type <instruccion> insert select delete /*update*/
%type <num_elementos> lista_creacion lista_expr lista_lista_expr
%type <num_elementos> lista_ids seleccion
/*%type <instruccion> lista_sets
%type <instruccion> def_referencias
%type <instruccion> set
%type <instruccion> booleano booleano_primario comparacion
%type <instruccion> predicado bit_expr simple_expr*/

/* Precedencia y asociatividad */
%left '>' '<' '=' MAIG MEIG DIST
%left OR XOR
%left AND
%left NOT
%left '%'
%left '+' '-'
%left '*' '/'
%right '^'

%%

entrada:			/* nada */
	|				entrada '\n'
						{ prompt(); }
	|				entrada
						{ dentro_de_definicion = 1; }
					definicion
						{ dentro_de_definicion = 0;
						  codificar(1, DETENER); return 1; }
	|				entrada
					{ dentro_de_definicion = 1; }
					manipulacion
					{ dentro_de_definicion = 0;
					  codificar(1, DETENER); return 1; }
	|				entrada error
						{ yyerrok; prompt(); }
	;

definicion:			create_database
						{ $$ = $1; }
	|				drop_database
						{ $$ = $1; }
	|				rename_database
						{ $$ = $1; }
	|				use_database
						{ $$ = $1; }
	|				create_table
						{ $$ = $1; }
	|				drop_table
						{ $$ = $1; }
	|				rename_table
						{ $$ = $1; }
	|				alter_table
						{ $$ = $1; }
	|				show_databases
						{ $$ = $1; }
	|				show_tables
						{ $$ = $1; }
	|				show_columns
						{ $$ = $1; }
	;

manipulacion:		select
						{ $$ = $1; }
	|				insert
						{ $$ = $1; }
//	|				update
	|				delete
						{ $$ = $1; }
	;

/* DEFINICION ****************************************************************/

create_database:	CREATE DATABASE ID ';'
						{ $$ = codificar(2, crear_base, (Instruccion) $3); }
	;

drop_database:		DROP DATABASE ID ';'
						{ $$ = codificar(2, borrar_base, (Instruccion) $3); }
	;

rename_database:	RENAME DATABASE ID TO ID ';'
						{ $$ = codificar(3, renombrar_base, (Instruccion) $3,
						  	(Instruccion) $5); }
	;

use_database:		USE ID ';'
						{ $$ = codificar(2, seleccionar_base,
							(Instruccion) $2); }
	;

create_table:		CREATE TABLE ID '(' lista_creacion ')' ';'
						{ $$ = codificar(3, crear_tabla, (Instruccion) $3,
							(long int) $5); }
	;

drop_table:			DROP TABLE ID ';'
						{ $$ = codificar(2, borrar_tabla, (Instruccion) $3); }
	;

rename_table:		RENAME TABLE ID TO ID ';'
						{ $$ = codificar(3, renombrar_tabla, (Instruccion) $3,
						  	(Instruccion) $5); }
	;

alter_table:		ALTER TABLE ID
						{ $<instruccion>$ = codificar(2, codigo_alter,
							(Instruccion) $3); }
					alter ';'
						{ $$ = $5; }
	;

show_databases:		SHOW DATABASES ';'
						{ $$ = codificar(1, mostrar_bases); }
	;

show_tables:		SHOW TABLES ';'
						{ $$ = codificar(1, mostrar_tablas); }
	;

show_columns:		SHOW COLUMNS FROM ID ';'
						{ $$ = codificar(2, mostrar_columnas,
							(Instruccion) $4); }
	;

lista_creacion:		/* nada */
						{ $$ = 0; }
	|				def_creacion
						{ $$ = 1; }
	|				lista_creacion ',' def_creacion
						{ $$ = $1 + 1; }
	;

def_creacion:		ID def_columna
						{ $$ = codificar(2, meter_cadena, $1); }
//	|				PRIMARY KEY '(' lista_ids ')'
//	|				FOREIGN KEY '(' lista_ids ')' def_referencias
	;


lista_ids:			ID
						{ $$ = 1; codificar(2, meter_cadena,
							(Instruccion) $1); }
	|				lista_ids ',' ID
						{ $$ = $1 + 1; codificar(2, meter_cadena,
							(Instruccion) $3); }
	;

/*
def_referencias:	REFERENCES ID '(' lista_ids ')'
	;
*/

alter:				ADD COLUMN ID def_columna
						{ $$ = codificar(2, agregar_campo, (Instruccion) $3); }
	|				CHANGE COLUMN ID ID def_columna
						{ $$ = codificar(3, renombrar_campo, (Instruccion) $3,
						  	(Instruccion) $4); }
//	|				MODIFY COLUMN ID def_columna
	|				DROP COLUMN ID
						{ $$ = codificar(2, borrar_campo, (Instruccion) $3); }
//	|				DROP PRIMARY KEY
//	|				DROP FOREIGN KEY ID
	;


def_columna:		tipo_dato
						{ $$ = $1; }
//	|				tipo_dato PRIMARY KEY
	;

tipo_dato:			/*INT
	|				DOUBLE
	|				CHAR
	|*/				VARCHAR '(' NUMERO ')'
						{ ; }
	;

/* MANIPULACION **************************************************************/

select:				SELECT seleccion FROM ID ';'
						{ $$ = codificar(3, codigo_select_simple,
							(Instruccion) $4, (long int) $2); }
//	|				SELECT seleccion FROM ID WHERE expresion
	;


insert:				INSERT INTO ID VALUES '(' lista_expr ')' ';'
						{ $$ = codificar(3, codigo_insert_simple,
							(Instruccion) $3, (long int) $6); }
	|				INSERT INTO ID VALUES '(' lista_lista_expr ')' ';'
						{ $$ = codificar(3, codigo_insert_compuesto,
							(Instruccion) $3, (long int) $6); }
	;

/*
update:				UPD ATE ID SET lista_sets
	|				UPDATE ID SET lista_sets WHERE expresion
	;
*/

 /* Modificación a la gramática original para hacer deletes más simples. */

delete:				DELETE FROM ID WHERE ID '=' expresion ';'
						{ $$ = codificar(3, codigo_delete, (Instruccion) $3,
							(Instruccion) $5); }
	;

seleccion:			lista_ids
						{ $$ = $1; }
	|				'*'
						{ $$ = -1; }
	;


/* En la gramática original CADENA no va aquí */

expresion:			CADENA
						{ $$ = codificar(2, meter_cadena, $1); }
/*	|				expresion OR expresion
	|				expresion AND expresion
	|				expresion XOR expresion
	|				NOT expresion
	|				booleano_primario IS booleano
	|				booleano_primario IS NOT booleano
	|				booleano_primario*/
	;

/*
lista_sets:			set
	|				set ',' lista_sets
	;

set:				ID '=' expresion
	|				ID '=' DEFAULT
	;
*/

lista_expr:			/* nada */
						{ $$ = 0; }
	|				expresion
						{ $$ = 1; }
	|				expresion ',' lista_expr
						{ $$ = $3 + 1; }
	;

lista_lista_expr:	'{' lista_expr '}'
						{ $$ = 1; codificar(2, meter_entero, (long int) $2); }
	|				lista_lista_expr ',' '{' lista_expr '}'
						{ $$ = $1 + 1; codificar(2, meter_entero,
							(long int) $4); }
	;

/*
booleano:			BOL_TRUE
	|				BOL_FALSE
	;

booleano_primario:	booleano_primario comparacion predicado
	|				predicado
	;

comparacion:		'='
	|				'<'
	|				'>'
	|				MEIG
	|				MAIG
	|				DIST
	;

predicado:			bit_expr IN '(' lista_expr ')'
	|				bit_expr NOT IN '(' lista_expr ')'
	|				bit_expr BETWEEN bit_expr AND predicado
	|				bit_expr NOT BETWEEN bit_expr AND predicado
	|				bit_expr LIKE simple_expr
	|				bit_expr NOT LIKE simple_expr
	|				bit_expr
	;

bit_expr:			bit_expr '|' bit_expr
	|				bit_expr '&' bit_expr
	|				bit_expr CD bit_expr
	|				bit_expr CI bit_expr
	|				bit_expr '+' bit_expr
	|				bit_expr '-' bit_expr
	|				bit_expr '/' bit_expr
	|				bit_expr '*' bit_expr
	|				bit_expr '%' bit_expr
	|				bit_expr '^' bit_expr
	|				simple_expr
	;

simple_expr:		ID
	|				NUMERO
	|				NUMERO_FLOTANTE
	|				CADENA
	;
*/
%%

void yyerror (char *mensaje)
{
	dentro_de_definicion = 0;
    error_en_ejecucion(mensaje, (char *) 0);
}
