# Gestor de bases de datos SQL #

Implementación de un gestor de bases de datos y un intérprete SQL.


### Estructura de programa ###

* Implementación de intérprete (*front-end*): **analizador_léxico.l**, **analizador_sintáctico.y**, y **sql.c**.
* Instrucciones de programa (RAM y máquina de pila): **programa.c** y **símbolo.c**.
* Funciones del gestor (instrucciones sql): **operaciones_definicion.c** y **operaciones_manipulacion.c**

### Dependencias ###

* Bison 3.0.4
* Flex 2.6.4
* Biblioteca de **curses** (solo para linux); me parece que es *libcurses*.

### Formato de código ###

* Tabulares *duros* de 4 espacios.
* Codificación de archivo fuentes en utf-8.
* Llaves de funciones en nueva linea.
* Comentarios sólo con asteriscos.
* Comentarios con **TODO** para marcar pendientes.

Por ejemplo:
```
#!c

/* Comentatios */
void funcion1 ()
{
    for (int i=0; i<100; i++)
        printf("%d ", i);

    if (1) {
        funcion1();
        funcion2();
    }

}

/* TODO: falta implementar funcion2. */

```

### Recursos ###

* Documentación de gramática: https://dev.mysql.com/doc/refman/5.5/en/sql-syntax.html